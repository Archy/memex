#!/bin/bash
set -euxo pipefail

MEMEX_VERSION=1.0-SNAPSHOT # TODO extract from pom
BROKER_IMAGE_DIR=distribution/activemq-docker-image
AGENT_IMAGE_DIR=distribution/agent-docker-image
SEVER_IMAGE_DIR=distribution/server-docker-image

# prepare directories
[[ -d "${AGENT_IMAGE_DIR}/tmp" ]] && rm -rf "${AGENT_IMAGE_DIR}/tmp"
mkdir "${AGENT_IMAGE_DIR}/tmp"

[[ -d "${SEVER_IMAGE_DIR}/tmp" ]] && rm -rf "${SEVER_IMAGE_DIR}/tmp"
mkdir -p "${SEVER_IMAGE_DIR}/tmp/plugins"

# build MemeX
mvn clean install -T1C -Prelease -Dmemex.home.plugins="$(pwd)/${SEVER_IMAGE_DIR}/tmp/plugins"

# build agent
cp "memex-agent/target/memex-agent-${MEMEX_VERSION}.jar" "${AGENT_IMAGE_DIR}/tmp"
pushd "${AGENT_IMAGE_DIR}"
./build.sh
rm -rf tmp
popd

# build server
cp "memex-server/memex-webapp/target/memex-webapp-${MEMEX_VERSION}.war" "${SEVER_IMAGE_DIR}/tmp"
pushd "${SEVER_IMAGE_DIR}"
./build.sh
rm -rf tmp
popd

# build broker
pushd "${BROKER_IMAGE_DIR}"
./build.sh
popd
