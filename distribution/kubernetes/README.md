## How to deploy MemeX to a local Kubernetes cluster v1
* start cluster: `minikube start`
* build memex docker images re-using minicube docker:
    * `eval $(minikube docker-env)`
    * run `build.sh`
* run the deployment using 1 of available options described below
* delete cluster: `minikube delete`

### Deploying a bare Pod
* create the pod: `kubectl create -f memex.yaml`
    * check all pods: `kubectl get pod`
    * check created memex pod: `kubectl describe pod memex`
* expose port
    * forward port: `kubectl port-forward memex 8080:8080`
    * create service (see below)
* delete the pod: `kubectl delete -f memex.yaml`
    
### Deploying a Replication Controller:
* create a replication controller: `kubectl create -f memex-replication-controller.yaml`
    * delete pod to check if the RC works: `kubectl delete pod <pod name>`
    * increase replicas count: `kubectl scale --replicas=2 -f memex-replication-controller.yaml`
* delete controller: `kubectl delete rc/memex-rc`

### Deploying a Deployment
* create a deployment: `kubectl create -f memex-deployment.yaml`
* check deployment status:
    * `kubectl get deployments`
    * `kubectl get rs`
    * `kubectl rollout status deployment/memex-deployment`
* create and expose service for the deployment:
    * `kubectl expose deployment memex-deployment --type=NodePort`
    * get url: `minikube service memex-service --url`
* delete the deployment: `kubectl delete -f memex-deployment.yaml`

## Deploying full MemeX app
* `kubectl apply -f memex-full.yaml`
* `minikube service memex --url`
* Note that this is a toy deployment and it's not using persistent volumes yet

## Creating a Service for accessing MemeX
There are 2 options to create a Service:
* directly from kubectl: `kubectl expose pod memex --type=NodePort --name memex-service`
* with yaml: `kubectl create -f memex-service.yaml`

Then you can:
* check services: `kubectl get service`
* get url for accessing MemeX: `minikube service memex-service --url`

## Managing secrets in k8s:
* create secret: `kubectl create secret generic <secret name> `

## K8s tips and tricks
* run `source <(kubectl completion bash)` to enable `kubectl` autocompletion
* ssh to pod: `kubectl exec --stdin --tty <pod name> -- /bin/bash`