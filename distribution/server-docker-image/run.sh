#!/bin/bash
set -euxo pipefail

docker network create memex

docker run --name postgres --network memex --rm -e POSTGRES_DB=memex -e POSTGRES_USER=memex -e POSTGRES_PASSWORD=memex postgres:10-alpine

docker run --rm --name server --network memex -p 8080:8080 memex-server