#!/bin/bash
set -euxo pipefail

docker run --rm --name broker -p 8161:8161 -p 61616:61616 memex-broker
