FROM eclipse-temurin:17-jdk-focal
LABEL description="MemeX Broker"

ENV ACTIVEMQ_USER activemq
ENV ACTIVEMQ_GROUP activemq
ENV ACTIVEMQ_USER_HOME /home/${ACTIVEMQ_USER}
ENV ACTIVEMQ_INSTALL_DIR /opt/activemq
RUN set -x && \
    addgroup ${ACTIVEMQ_GROUP} && \
    adduser ${ACTIVEMQ_USER} --home ${ACTIVEMQ_USER_HOME} --ingroup ${ACTIVEMQ_GROUP} --disabled-password

RUN set -x && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        curl \
    && \
    rm -rf /var/lib/apt/lists/*

ARG ACTIVEMQ_VERSION=5.16.3
ARG ATIVEMQ_DOWNLOAD_URL=http://www.apache.org/dyn/closer.cgi?filename=/activemq/${ACTIVEMQ_VERSION}/apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz&action=download
RUN set -x && \
    mkdir -p ${ACTIVEMQ_INSTALL_DIR} && \
    curl -L "${ATIVEMQ_DOWNLOAD_URL}" | tar -xz --strip-components=1 -C "${ACTIVEMQ_INSTALL_DIR}" && \
    rm -rf "${ACTIVEMQ_INSTALL_DIR}/docs" && \
    rm -rf "${ACTIVEMQ_INSTALL_DIR}/examples" && \
    rm -rf "${ACTIVEMQ_INSTALL_DIR}/webapps" && \
    rm -rf "${ACTIVEMQ_INSTALL_DIR}/webapps-demo" && \
    rm -rf "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml" && \
    chown -R "${ACTIVEMQ_USER}:${ACTIVEMQ_GROUP}" "${ACTIVEMQ_INSTALL_DIR}"

USER ${ACTIVEMQ_USER}
WORKDIR ${ACTIVEMQ_INSTALL_DIR}

COPY  --chown=activemq:activemq activemq.xml ${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml

EXPOSE 61616
EXPOSE 8161
CMD ["/bin/sh", "-c", "bin/activemq console"]
