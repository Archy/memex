open module memex.agent {
    requires memex.shared.api;
    requires memex.shared.core;

    //Spring
    requires spring.beans;
    requires spring.core;
    requires spring.context;
    requires spring.jms;

    //ActiveMQ
    requires activemq.client;
    requires jms;
    requires java.naming;

    //Misc
    requires org.apache.logging.log4j;
    requires org.jetbrains.annotations;
    requires java.desktop;
}