package com.example.memex.agent;

import com.example.memex.agent.image.MemeProcessor;
import com.example.memex.agent.messaging.Agent;
import com.example.memex.shared.api.MemeSender;
import com.example.memex.shared.api.MemeXJMS;
import com.example.memex.shared.api.ProcessedMemeSender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MemexAgentConfig {
    @Bean
    public MemeProcessor memeProcessor() {
        return new MemeProcessor();
    }

    @Bean
    public Agent agent(final MemeProcessor memeProcessor,
                       final MemeSender memeSender,
                       final MemeXJMS memeXJMS,
                       final ProcessedMemeSender processedMemeSender) {
        return new Agent(memeProcessor, memeSender, memeXJMS, processedMemeSender);
    }

}
