package com.example.memex.agent;

import com.example.memex.shared.core.MemexSharedJmsConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * MemeX agent
 */
public class AgentRunner {

    public static void main(String[] args) {
        final ApplicationContext ctx = new AnnotationConfigApplicationContext(
                MemexAgentConfig.class, MemexSharedJmsConfig.class);
    }
}
