package com.example.memex.agent.image;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Memes images processing service
 */
public class MemeProcessor {
    private static final Logger log = LogManager.getLogger(MemeProcessor.class);

    /**
     * Add MemeX logo to the image
     * @param inputStream image to process
     * @return processed image
     */
    public byte[] processMeme(@NotNull final InputStream inputStream) {
        try {
            final BufferedImage image = ImageIO.read(inputStream);
            addMemeXTrademark(image);

            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", out);

            return out.toByteArray();
        } catch(IOException e) {
            log.error("Exception while processing an image", e);
            throw new RuntimeException(e);
        }
    }

    private static void addMemeXTrademark(BufferedImage image) {
        final Graphics g = image.getGraphics();
        try {
            final Font font = new Font("Arial", Font.BOLD, 30);
            final FontMetrics metrics = g.getFontMetrics(font);
            final int positionX = 0;
            final int positionY = (image.getHeight() - metrics.getHeight()) + metrics.getAscent();

            g.setFont(font);
            g.drawString("MemeX", positionX, positionY);
        } finally {
            g.dispose();
        }
    }
}
