package com.example.memex.agent.messaging;

import com.example.memex.agent.image.MemeProcessor;
import com.example.memex.shared.api.MemeSender;
import com.example.memex.shared.api.MemeXJMS;
import com.example.memex.shared.api.ProcessedMemeSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.InputStream;

public class Agent {
    private static final Logger log = LogManager.getLogger(Agent.class);

    private final MemeProcessor memeProcessor;
    private final MemeSender memeSender;
    private final MemeXJMS memeXJMS;
    private final ProcessedMemeSender processedMemeSender;

    public Agent(final MemeProcessor memeProcessor,
                 final MemeSender memeSender,
                 final MemeXJMS memeXJMS,
                 final ProcessedMemeSender processedMemeSender) {
        this.memeProcessor = memeProcessor;
        this.memeSender = memeSender;
        this.memeXJMS = memeXJMS;
        this.processedMemeSender = processedMemeSender;
    }

    @JmsListener(destination = MemeXJMS.MEME_QUEUE)
    void receive(final Message message) throws JMSException {
        final long memeId = memeXJMS.getMemeIdFromMessage(message);
        log.debug(String.format("Received processing request for meme %d (meessage id: %s", memeId, message.getJMSMessageID()));

        final InputStream meme = memeSender.getMeme(message);
        final byte[] result = memeProcessor.processMeme(meme);
        processedMemeSender.sendProcessedMeme(memeId, result);

        log.debug(String.format("Meme %d processed", memeId));
    }
}
