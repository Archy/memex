package com.example.memex.pageobjects.webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WebdriverValidationUtils {
    private static Duration DEFAULT_TIMEOUT = Duration.ofSeconds(30);

    public static void waitForElement(final WebDriver driver, final By by) {
        new WebDriverWait(driver, DEFAULT_TIMEOUT)
                .until(ExpectedConditions.numberOfElementsToBe(by, 1));
    }

    public static void waitForAtLeastOneElement(final WebDriver driver, final By by) {
        new WebDriverWait(driver, DEFAULT_TIMEOUT)
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(by, 0));
    }
}
