package com.example.memex.pageobjects.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class UniqueNameHelper {
    public static String makeUnique(final String name) {
        return name + RandomStringUtils.randomAlphanumeric(8);
    }
}
