package com.example.memex.pageobjects.pages;

import com.example.memex.pageobjects.components.HeaderComponent;
import com.example.memex.pageobjects.webdriver.WebdriverValidationUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class MemeXPage {
    private static final By MEME_BY = By.className("meme");
    private static final By MEME_TITLE_BY = By.cssSelector("div.meme h4");

    private final WebDriver driver;

    public MemeXPage(final WebDriver driver) {
        this.driver = driver;
        WebdriverValidationUtils.waitForAtLeastOneElement(driver, MEME_BY);
    }

    public HeaderComponent getHeader() {
        return HeaderComponent.getHeader(driver);
    }

    public List<String> getAllMemesTitles() {
        return driver.findElements(MEME_TITLE_BY).stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }
}
