package com.example.memex.pageobjects.pages;

import com.example.memex.pageobjects.components.HeaderComponent;
import com.example.memex.pageobjects.webdriver.WebdriverValidationUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    private static final By USERNAME_BY = By.name("username");
    private static final By PASSWORD_BY = By.name("password");
    private static final By BUTTON_BY = By.id("loginButton");

    private final WebDriver driver;

    public LoginPage(final WebDriver driver) {
        this.driver = driver;
        WebdriverValidationUtils.waitForElement(driver, USERNAME_BY);
    }

    public HeaderComponent getHeader() {
        return HeaderComponent.getHeader(driver);
    }

    public LoginPage withUsername(final String username) {
        driver.findElement(USERNAME_BY).sendKeys(username);
        return this;
    }

    public LoginPage withPassword(final String password) {
        driver.findElement(PASSWORD_BY).sendKeys(password);
        return this;
    }

    public MemeXPage submit() {
        driver.findElement(BUTTON_BY).click();
        return new MemeXPage(driver);
    }
}
