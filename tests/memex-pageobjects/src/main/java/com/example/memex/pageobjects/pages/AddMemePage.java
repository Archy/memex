package com.example.memex.pageobjects.pages;

import com.example.memex.pageobjects.components.HeaderComponent;
import com.example.memex.pageobjects.webdriver.WebdriverValidationUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.File;

public class AddMemePage {
    private static final By TITLE_BY = By.name("title");
    private static final By FILE_BY = By.name("image");
    private static final By BUTTON_BY = By.id("addButton");

    private final WebDriver driver;

    public AddMemePage(final WebDriver driver) {
        this.driver = driver;
        WebdriverValidationUtils.waitForElement(driver, TITLE_BY);
    }

    public HeaderComponent getHeader() {
        return HeaderComponent.getHeader(driver);
    }

    public AddMemePage withTitle(final String title) {
        driver.findElement(TITLE_BY).sendKeys(title);
        return this;
    }

    public AddMemePage withImage(final File image) {
        driver.findElement(FILE_BY).sendKeys(image.getAbsolutePath());
        return this;
    }

    public MemeXPage submit() {
        driver.findElement(BUTTON_BY).click();
        return new MemeXPage(driver);
    }
}
