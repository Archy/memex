package com.example.memex.pageobjects.components;

import com.example.memex.pageobjects.pages.AddMemePage;
import com.example.memex.pageobjects.pages.LoginPage;
import com.example.memex.pageobjects.pages.RegisterPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HeaderComponent {
    private static final By HEADER_BY = By.id("memexHeader");
    private static final By ADD_MEME_BY = By.id("addMemeLink");
    private static final By LOGIN_BY = By.id("loginLink");
    private static final By REGISTER_BY = By.id("registerLink");

    private final WebDriver driver;
    private final WebElement headerElement;

    public static HeaderComponent getHeader(final WebDriver driver) {
        return new HeaderComponent(driver, driver.findElement(HEADER_BY));
    }

    private HeaderComponent(final WebDriver driver, final WebElement headerElement) {
        this.driver = driver;
        this.headerElement = headerElement;
    }

    public AddMemePage addMeme() {
        headerElement.findElement(ADD_MEME_BY).click();
        return new AddMemePage(driver);
    }

    public LoginPage login() {
        headerElement.findElement(LOGIN_BY).click();
        return new LoginPage(driver);
    }

    public RegisterPage register() {
        headerElement.findElement(REGISTER_BY).click();
        return new RegisterPage(driver);
    }
}
