package com.example.memex.webdriver.test;

import com.example.memex.pageobjects.utils.UniqueNameHelper;
import com.example.memex.webdriver.MemeXFunctionalTest;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class MemexIT extends MemeXFunctionalTest {

    @Test
    void testMemeX() throws URISyntaxException {
        final String username = UniqueNameHelper.makeUnique("joe");
        final String password = "joe123";

        final String title =  UniqueNameHelper.makeUnique("meme");
        final File memeFile = getMemeFile();

        goToMemex()
                .getHeader()
                .register()
                .withUsername(username)
                .withPassword(password)
                .submit()
                .getHeader()
                .login()
                .withUsername(username)
                .withPassword(password)
                .submit()
                .getHeader()
                .addMeme()
                .withTitle(title)
                .withImage(memeFile)
                .submit();


        final List<String> memes = goToMemex().getAllMemesTitles();
        assertAll(
                () -> assertFalse(memes.isEmpty()),
                () -> assertEquals(title + " your mom", memes.get(0))
        );
    }

    private File getMemeFile() throws URISyntaxException {
        final URL url = MemexIT.class.getClassLoader().getResource("meme.jpg");
        return new File(url.toURI());
    }
}
