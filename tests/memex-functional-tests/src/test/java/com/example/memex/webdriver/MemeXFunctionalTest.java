package com.example.memex.webdriver;

import com.example.memex.pageobjects.pages.MemeXPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class MemeXFunctionalTest {
    private static final String DEFAULT_URL = "http://localhost:8080/";
    private static final String MEMEX_URL = System.getProperty("memex.url", DEFAULT_URL);

    protected WebDriver driver;

    /**
     * Sets up WebDriver, This method must not be overridden in inheriting class.
     */
    @BeforeEach
    void setUpWebDriver() {
        driver = new FirefoxDriver();

    }

    /**
     * Cleans up WebDriver, This method must not be overridden in inheriting class.
     */
    @AfterEach
    void cleanUpWebDriver() {
        driver.close();
    }

    protected MemeXPage goToMemex() {
        driver.get(MEMEX_URL);
        return new MemeXPage(driver);
    }
}
