package com.example.memex.shared.core;

import com.example.memex.shared.api.MemeXJMS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 * MemeX JMS Utils
 */
public class MemeXJMSImpl implements MemeXJMS {
    private static final Logger log = LogManager.getLogger(MemeXJMSImpl.class);

    private static final String MEME_ID = "memeId";
    private static final String CLIENT_ID = "clientId";

    private final String clientId;

    public MemeXJMSImpl(final String clientId) {
        this.clientId = clientId;
    }

    @Override
    public void addMemeIdToMessage(final long memeId, final Message message) {
        try {
            message.setLongProperty(MEME_ID, memeId);
        } catch (JMSException e) {
            log.error("Error while adding meme id to message", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public long getMemeIdFromMessage(Message message) {
        try {
            return message.getLongProperty(MEME_ID);
        } catch (JMSException e) {
            log.error("Error while getting meme id from message", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addClientId(final Message message) {
        try {
            message.setStringProperty(CLIENT_ID, clientId);
        } catch (JMSException e) {
            log.error("Error while adding client id to message", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isOwnMessage(final Message message) {
        try {
            return clientId.equals(message.getStringProperty(CLIENT_ID));
        } catch (JMSException e) {
            log.error("Error while getting client id from message", e);
            throw new RuntimeException(e);
        }
    }
}
