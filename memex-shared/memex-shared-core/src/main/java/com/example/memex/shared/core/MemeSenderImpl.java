package com.example.memex.shared.core;

import com.example.memex.shared.api.MemeSender;
import com.example.memex.shared.api.MemeXJMS;
import com.example.memex.shared.core.utils.Narrow;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.BlobMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class MemeSenderImpl implements MemeSender {
    private static final Logger log = LogManager.getLogger(MemeSenderImpl.class);

    private final JmsTemplate memeJmsTemplate;
    private final MemeXJMS memeXJMS;

    public MemeSenderImpl(final JmsTemplate memeJmsTemplate, final MemeXJMS memeXJMS) {
        this.memeJmsTemplate = memeJmsTemplate;
        this.memeXJMS = memeXJMS;
    }

    @Override
    public void sendMeme(final long memeId, @NotNull final URL memeDownloadUrl) {
        log.debug(String.format("Sending meme %d for processing", memeId));
        log.debug(String.format("Meme download url is %s", memeDownloadUrl));
        memeJmsTemplate.send(session -> {
            final ActiveMQSession activeMQSession = Narrow.downTo(session, ActiveMQSession.class);
            if (activeMQSession == null) {
                final String msg = "Can't send meme since session is not an ActiveMQ session";
                log.error(msg);
                throw new RuntimeException(msg);
            }

            final Message blobMessage = activeMQSession.createBlobMessage(memeDownloadUrl);
            memeXJMS.addMemeIdToMessage(memeId, blobMessage);
            return blobMessage;
        });
        log.debug(String.format("Meme %d send for processing", memeId));
    }

    @Override
    public InputStream getMeme(final Message message) {
        final BlobMessage blobMessage = Narrow.downTo(message, BlobMessage.class);
        if (blobMessage == null) {
            final String msg = "Can't load meme since message is not a blob message";
            log.error(msg);
            throw new RuntimeException(msg);
        }

        return getMemeWithDelayedRetry(blobMessage);
    }

    private InputStream getMemeWithDelayedRetry(@NotNull final BlobMessage blobMessage) {
        try {
            log.debug(String.format("Meme download url is %s", blobMessage.getURL()));
            return blobMessage.getInputStream();
        } catch(IOException | JMSException e) {
            log.error("Exception while reading meme input stream. Retrying in 10s", e);

            //retry in 30 seconds
            try {
                Thread.sleep(10000);
                return blobMessage.getInputStream();
            } catch(IOException | JMSException | InterruptedException e2) {
                log.error("Second exception while reading meme input stream", e2);
                throw new RuntimeException(e2);
            }
        }
    }

}
