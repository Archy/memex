package com.example.memex.shared.core;

import com.example.memex.shared.api.MemeXJMS;
import com.example.memex.shared.api.ProcessedMemeSender;
import com.example.memex.shared.core.utils.Narrow;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;

public class ProcessedMemeSenderImpl implements ProcessedMemeSender {
    private static final Logger log = LogManager.getLogger(ProcessedMemeSenderImpl.class);
    private static final String MEME_SIZE = "memeSize";

    private final JmsTemplate processedMemeJmsTemplate;
    private final MemeXJMS memeXJMS;

    public ProcessedMemeSenderImpl(final JmsTemplate processedMemeJmsTemplate, final MemeXJMS memeXJMS) {
        this.processedMemeJmsTemplate = processedMemeJmsTemplate;
        this.memeXJMS = memeXJMS;
    }

    @Override
    public void sendProcessedMeme(final long memeId, final byte[] meme) {
        log.debug(String.format("Sending processed meme %d", memeId));
        processedMemeJmsTemplate.send(session -> {
            final BytesMessage message = session.createBytesMessage();
            message.writeBytes(meme);
            message.setIntProperty(MEME_SIZE, meme.length);
            memeXJMS.addMemeIdToMessage(memeId, message);
            return message;
        });
        log.debug(String.format("Processed meme %d send back", memeId));
    }

    @Override
    public byte[] getMeme(@NotNull final Message message) {
        final BytesMessage bytesMessage = Narrow.downTo(message, BytesMessage.class);
        if (bytesMessage == null) {
            final String msg = "Can't load meme since message is not a stream message";
            log.error(msg);
            throw new RuntimeException(msg);
        }

        try {
            final int memeSize = message.getIntProperty(MEME_SIZE);
            final byte[] bytes = new byte[memeSize];
            bytesMessage.readBytes(bytes);
            return bytes;

        } catch(JMSException e) {
            log.error("Exception while reading meme bytes", e);
            throw new RuntimeException(e);
        }
    }

}
