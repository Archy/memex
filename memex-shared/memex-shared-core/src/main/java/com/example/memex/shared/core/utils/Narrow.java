package com.example.memex.shared.core.utils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Narrow {

    @Nullable
    public static <X, T extends X> T downTo(@NotNull X o, @NotNull Class<T> clazz) {
        if (clazz.isAssignableFrom(o.getClass())) {
            return clazz.cast(o);
        }
        return null;
    }
}
