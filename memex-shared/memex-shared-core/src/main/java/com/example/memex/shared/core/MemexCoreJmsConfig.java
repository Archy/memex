package com.example.memex.shared.core;

import org.apache.activemq.command.ActiveMQTopic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;

@Configuration
@PropertySource("classpath:memex-shared.properties")
@EnableJms
@Profile("!tests")
public class MemexCoreJmsConfig {
    private static final Logger log = LogManager.getLogger(MemexCoreJmsConfig.class);

    @Bean
    public Destination cacheTopic() {
        return new ActiveMQTopic(MemeXJMSImpl.CACHE_SYNC_TOPIC);
    }

    @Bean("cacheInvalidationJmsTemplate")
    public JmsTemplate cacheInvalidationJmsTemplate(final ConnectionFactory connectionFactory, final Destination cacheTopic) {
        final JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
        jmsTemplate.setDefaultDestination(cacheTopic);

        return jmsTemplate;
    }

    @Bean("cacheInvalidationJmsListenerContainerFactory")
    public JmsListenerContainerFactory<DefaultMessageListenerContainer> jmsListenerContainerFactory(final ConnectionFactory connectionFactory,
                                                                                                    final String jmsClientId) {
        final DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

        factory.setConnectionFactory(connectionFactory);
        factory.setConcurrency("1-1");
        factory.setSubscriptionDurable(true);

        factory.setClientId(jmsClientId);
        log.debug("JMS client id: " + jmsClientId);

        return factory;
    }


}
