package com.example.memex.shared.core;

import com.example.memex.shared.api.MemeSender;
import com.example.memex.shared.api.MemeXJMS;
import com.example.memex.shared.api.ProcessedMemeSender;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import java.util.Random;

@Configuration
@PropertySource("classpath:memex-shared.properties")
@EnableJms
@Profile("!tests")
public class MemexSharedJmsConfig {
    private static final Logger log = LogManager.getLogger(MemexSharedJmsConfig.class);

    @Value("${memex.broker.url}")
    private String brokerUrl;

    @Bean
    public String jmsClientId() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;

        final Random random = new Random();
        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        log.info("Initializing JMS");
        log.debug(String.format("Broker url: %s", brokerUrl));

        final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
        connectionFactory.setWatchTopicAdvisories(false);

        return connectionFactory;
    }

    @Bean
    public Destination memeQueue() {
        return new ActiveMQQueue(MemeXJMS.MEME_QUEUE);
    }

    @Bean
    public Destination processedMemeQueue() {
        return new ActiveMQQueue(MemeXJMS.PROCESSED_MEME_QUEUE);
    }

    @Bean
    public JmsListenerContainerFactory<DefaultMessageListenerContainer> jmsListenerContainerFactory(final ConnectionFactory connectionFactory) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setConcurrency("2-2");
        return factory;
    }

    @Bean("memeJmsTemplate")
    public JmsTemplate memeJmsTemplate(final ConnectionFactory connectionFactory, final Destination memeQueue) {
        final JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
        jmsTemplate.setDefaultDestination(memeQueue);
        return jmsTemplate;
    }

    @Bean("processedMemeJmsTemplate")
    public JmsTemplate processedMemeJmsTemplate(final ConnectionFactory connectionFactory, final Destination processedMemeQueue) {
        final JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
        jmsTemplate.setDefaultDestination(processedMemeQueue);

        return jmsTemplate;
    }

    @Bean
    public MemeXJMS memeXJMS(final String jmsClientId) {
        return new MemeXJMSImpl(jmsClientId);
    }

    @Bean
    public MemeSender memeSender(final JmsTemplate memeJmsTemplate, final MemeXJMS memeXJMS) {
        return new MemeSenderImpl(memeJmsTemplate, memeXJMS);
    }

    @Bean
    public ProcessedMemeSender processedMemeSender(final JmsTemplate processedMemeJmsTemplate, final MemeXJMS memeXJMS) {
        return new ProcessedMemeSenderImpl(processedMemeJmsTemplate, memeXJMS);
    }

}
