open module memex.shared.core {
    exports com.example.memex.shared.core;

    requires memex.shared.api;

    //Spring
    requires spring.beans;
    requires spring.context;
    requires spring.jms;

    //ActiveMQ
    requires activemq.client;
    requires jms;
    requires java.naming;

    //Misc
    requires org.apache.logging.log4j;
    requires org.jetbrains.annotations;
}