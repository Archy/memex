package com.example.memex.shared.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.Message;

/**
 * Helper service for sending processed memes back to server
 */
public interface ProcessedMemeSender {
    /**
     * Send processed meme back
     */
    void sendProcessedMeme(final long memeId, @NotNull byte[] meme);

    /**
     * Get processed meme data from JMS message
     */
    byte[] getMeme(@NotNull Message message);
}
