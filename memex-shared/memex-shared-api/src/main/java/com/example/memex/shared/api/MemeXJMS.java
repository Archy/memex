package com.example.memex.shared.api;

import javax.jms.Message;

public interface MemeXJMS {
    String MEME_QUEUE = "com.example.memex.memeQueue";
    String PROCESSED_MEME_QUEUE = "com.example.memex.processedMmeQueue";
    String CACHE_SYNC_TOPIC = "com.example.memex.cacheTopic";

    void addMemeIdToMessage(final long memeId, final Message message);

    long getMemeIdFromMessage(final Message message);

    void addClientId(Message message);

    boolean isOwnMessage(Message message);
}
