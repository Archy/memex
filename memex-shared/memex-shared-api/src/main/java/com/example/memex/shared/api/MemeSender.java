package com.example.memex.shared.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.Message;
import java.io.InputStream;
import java.net.URL;

/**
 * Helper service for sending memes for processing
 */
public interface MemeSender {
    /**
     * Send meme to and agent for processing
     */
    void sendMeme(long memeId, @NotNull URL memeDownloadUrl);

    /**
     * Get meme data for processing from JMS message
     */
    InputStream getMeme(Message message);
}
