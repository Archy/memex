open module memex.shared.api {
    exports com.example.memex.shared.api;

    //ActiveMQ
    requires jms;

    //Misc
    requires org.apache.logging.log4j;
    requires org.jetbrains.annotations;
}