Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

$MEMEX_VERSION="1.0-SNAPSHOT" # TODO extract from pom
$BROKER_IMAGE_DIR="distribution\activemq-docker-image"
$PROXY_IMAGE_DIR="distribution\proxy-docker-image"
$AGENT_IMAGE_DIR="distribution\agent-docker-image"
$SEVER_IMAGE_DIR="distribution\server-docker-image"

prepare directories
Remove-Item -LiteralPath "${AGENT_IMAGE_DIR}\tmp" -Force -Recurse -ErrorAction Ignore
New-Item -Path "${AGENT_IMAGE_DIR}" -Name "tmp" -ItemType "directory"

Remove-Item -LiteralPath "${SEVER_IMAGE_DIR}\tmp" -Force -Recurse -ErrorAction Ignore
New-Item -Path "${SEVER_IMAGE_DIR}" -Name "tmp" -ItemType "directory"


build MemeX
mvn clean install -T1C -Prelease "-Dmemex.home.plugins=$pwd\${SEVER_IMAGE_DIR}\tmp\plugins"


build agent
Copy-Item "memex-agent/target/memex-agent-${MEMEX_VERSION}.jar" -Destination "${AGENT_IMAGE_DIR}/tmp"
Push-Location "${AGENT_IMAGE_DIR}"
docker build -t memex-agent .
Pop-Location

build server
Copy-Item "memex-server/memex-webapp/target/memex-webapp-${MEMEX_VERSION}.war" -Destination "${SEVER_IMAGE_DIR}/tmp"
Push-Location "${SEVER_IMAGE_DIR}"
docker build -t memex-server .
Pop-Location

build broker
Push-Location "${BROKER_IMAGE_DIR}"
docker build -t memex-broker .
Pop-Location