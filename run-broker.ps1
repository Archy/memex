Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

$BROKER_IMAGE_DIR="distribution\activemq-docker-image"


Push-Location "${BROKER_IMAGE_DIR}"

docker build -t memex-broker .

docker run --rm --name broker -p 8161:8161 -p 61616:61616 memex-broker

Pop-Location