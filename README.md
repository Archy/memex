# MemeX
WebApp for memes

The aim of this project it to use multiple different technologies in a single code base. The functionality is kept as small as possible, and some problems have been introduced artificially to allow for more comprehensive usage of technologies.  

## Set up running from Intellij:
* add `Tomcat Server 9.x` run configuration
* set `URL` to `http://localhost:8080`
* set `Vm options` to `-DMEMEX_HOME_DIR` to memex home dir
* set `JRE` to Java 11
* add MemeX artifact (`memex-webapp:war exploded`) to the `Deploy at the server startup` field
* set `Application context` to `` (nothing)

## Manual deploy to Tomcat
* use `sever.xml` from `distribution/server-docker-image`
* copy memex war to `CATALINA_HOME` dir (not the webapps dir!)
* add environment variable `MEMEX_HOME_DIR` and set it to memex home directory
* set `CATALINA_OPTS` environment variable to `"-Dspring.profiles.active=tests -Dmemex.server.war=${MEMEX_SERVER_WAR}"`
* start Tomcat: `${CATALINA_HOME}/bin/catalina.sh run`

## Running tests
* to run WebDriver tests you need to have path to directory with driver in `PATH` environment variable 
or pass `webdriver.gecko.driver` system property
```
mvn verify -pl :memex-functional-tests -PfunctionalTests -Denforcer.skip=true -Dmemex.url=http://localhost:8080/memex-webapp -Dtomcat.home=C:\Development\apache-tomcat-9.0.24-2 -Dwebdriver.gecko.driver=C:\Development\webdriver\geckodriver.exe -Dmemex.home=D:\memex -Dpostgres.host=localhost
```
