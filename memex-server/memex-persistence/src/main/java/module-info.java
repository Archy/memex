open module memex.persistence {
    requires memex.core;

    //JavaEE
    requires java.persistence;
    requires java.sql;

    //Misc
    requires org.apache.logging.log4j;
    requires org.jetbrains.annotations;
}