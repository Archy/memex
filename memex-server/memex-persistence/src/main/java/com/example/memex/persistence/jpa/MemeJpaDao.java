package com.example.memex.persistence.jpa;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.meme.MemeDao;
import com.example.memex.persistence.model.meme.MemeImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import java.util.List;

public class MemeJpaDao implements MemeDao {
    private static final Logger log = LogManager.getLogger(MemeJpaDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Meme save(@NotNull String title, @NotNull String fileName, @NotNull String accessToken) {
        final Meme meme = new MemeImpl(title, fileName, accessToken);
        entityManager.persist(meme);

        return meme;
    }

    @Override
    public void update(@NotNull final Meme meme) {
        entityManager.merge(meme);
        entityManager.flush();
    }

    @Override
    @NotNull
    public List<Meme> findAll() {
        return entityManager.createNamedQuery(MemeImpl.FIND_ALL, Meme.class).getResultList();
    }

    @Override
    public Meme findById(final long id) {
        try {
            final TypedQuery<Meme> query = entityManager.createNamedQuery(MemeImpl.FIND_BY_ID, Meme.class);
            query.setParameter("id", id);

            return query.getSingleResult();
        } catch (NoResultException e) {
            log.trace(e);
            return null;
        }
    }

    public void deleteAll() {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaDelete<MemeImpl> c = cb.createCriteriaDelete(MemeImpl.class);
        final Root<MemeImpl> ignored = c.from(MemeImpl.class);

        try {
            entityManager.createQuery(c).executeUpdate();
        } catch (NoResultException e) {
            log.trace(e);
        }
    }
}
