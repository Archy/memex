package com.example.memex.persistence.jpa.converter;

import com.example.memex.api.vote.VoteType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;

@Converter(autoApply = true)
public class VoteTypeConverter implements AttributeConverter<VoteType, Long> {
    @Override
    public Long convertToDatabaseColumn(final VoteType attribute) {
        return attribute.getId();
    }

    @Override
    public VoteType convertToEntityAttribute(final Long dbData) {
        return Arrays.stream(VoteType.values())
                .filter(voteType -> voteType.getId() == dbData)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("Cannot vote type for code %d", dbData)));
    }
}
