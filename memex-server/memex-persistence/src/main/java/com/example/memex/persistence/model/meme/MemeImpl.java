package com.example.memex.persistence.model.meme;

import com.example.memex.api.meme.Meme;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "MEME")
@NamedQueries({
        @NamedQuery(name = MemeImpl.FIND_ALL,
                query = "select meme from MemeImpl meme order by meme.id desc"),
        @NamedQuery(name = MemeImpl.FIND_BY_ID,
                query = "select meme from MemeImpl meme where meme.id = :id")
})
public class MemeImpl implements Meme {
    public static final String FIND_ALL = "meme.findAll";
    public static final String FIND_BY_ID = "meme.findById";

    @Id
    @Column(name = "MEME_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "TITLE", nullable = false)
    private String title;
    @Column(name = "FILENAME", nullable = false, unique = true)
    private String fileName;
    @Column(name = "TOKEN", unique = true)
    private String  accessToken;
    @Column(name = "VISIBLE", nullable = false)
    private Boolean visible;

    public MemeImpl() {
    }

    public MemeImpl(@NotNull final String title, @NotNull final String fileName, String accessToken) {
        this.title = title;
        this.fileName = fileName;
        this.accessToken = accessToken;
        this.visible = false;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    @NotNull
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(@NotNull final String title) {
        this.title = title;
    }

    @Override
    @NotNull
    public String getFileName() {
        return fileName;
    }

    @Override
    public void setFileName(@NotNull final String fileName) {
        this.fileName = fileName;
    }

    @Override
    @Nullable
    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public void setAccessToken(@Nullable String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public void makeVisible() {
        this.visible = true;
        this.accessToken = null;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MemeImpl meme = (MemeImpl) o;
        return Objects.equals(id, meme.id) && Objects.equals(title, meme.title) && Objects.equals(fileName,
                                                                                                  meme.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, fileName);
    }
}
