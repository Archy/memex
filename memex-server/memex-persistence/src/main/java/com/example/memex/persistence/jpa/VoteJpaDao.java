package com.example.memex.persistence.jpa;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.user.User;
import com.example.memex.api.vote.Vote;
import com.example.memex.api.vote.VoteDao;
import com.example.memex.api.vote.VoteType;
import com.example.memex.persistence.model.vote.VoteImpl;
import com.example.memex.persistence.model.vote.VoteImpl_;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Objects;

public class VoteJpaDao implements VoteDao {
    private static final Logger log = LogManager.getLogger(VoteJpaDao.class);

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public long countVotes(final long memeId) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        final CriteriaQuery<Long> c = cb.createQuery(Long.class);
        final Root<VoteImpl> vote = c.from(VoteImpl.class);

        c.select(
                cb.sum(
                        cb.<Long>selectCase()
                                .when(cb.equal(vote.get(VoteImpl_.TYPE), VoteType.UPVOTE), 1L)
                                .when(cb.equal(vote.get(VoteImpl_.TYPE), VoteType.DOWNVOTE), -1L)
                                .otherwise(0L)
                )
        ).where(cb.equal(vote.get(VoteImpl_.MEME), memeId));

        final Long result = entityManager.createQuery(c).getSingleResult();
        log.debug(String.format("Vote count for meme %d is %d", memeId, result));
        return Objects.requireNonNullElse(result, 0L);
    }

    @Override
    public Vote save(@NotNull User user, @NotNull Meme meme, @NotNull VoteType voteType) {
        final Vote vote = new VoteImpl(user, meme, voteType);
        save(vote);

        return vote;
    }

    @Override
    public void save(@NotNull final Vote vote) {
        entityManager.persist(vote);
    }

    @Override
    public boolean isVoteAlreadyCasted(final @NotNull User user, final @NotNull Meme meme) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        final CriteriaQuery<Integer> c = cb.createQuery(Integer.class);
        final Root<VoteImpl> vote = c.from(VoteImpl.class);
        c
                .select(vote.get(VoteImpl_.ID))
                .where(cb.and(
                        cb.equal(vote.get(VoteImpl_.USER), user),
                        cb.equal(vote.get(VoteImpl_.MEME), meme)
                ));
        return !entityManager.createQuery(c).getResultList().isEmpty();
    }

    @Override
    public void removeVote(final @NotNull User user, final @NotNull Meme meme) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        final CriteriaDelete<VoteImpl> c = cb.createCriteriaDelete(VoteImpl.class);
        final Root<VoteImpl> vote = c.from(VoteImpl.class);
        c.where(cb.and(
                cb.equal(vote.get(VoteImpl_.USER), user),
                cb.equal(vote.get(VoteImpl_.MEME), meme)
        ));

        final int result = entityManager.createQuery(c).executeUpdate();
        log.debug(String.format("Deleted %d votes", result));
    }

    public void deleteAll() {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaDelete<VoteImpl> c = cb.createCriteriaDelete(VoteImpl.class);
        final Root<VoteImpl> ignored = c.from(VoteImpl.class);

        try {
            entityManager.createQuery(c).executeUpdate();
        } catch (NoResultException e) {
            log.trace(e);
        }
    }
}
