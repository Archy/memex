package com.example.memex.persistence.model.vote;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.user.User;
import com.example.memex.api.vote.Vote;
import com.example.memex.api.vote.VoteType;
import com.example.memex.persistence.model.meme.MemeImpl;
import com.example.memex.persistence.model.user.UserImpl;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "VOTE",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"USER_ID", "MEME_ID"})
        })
public class VoteImpl implements Vote {
    @Id
    @Column(name = "VOTE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = UserImpl.class)
    @JoinColumn(name = "USER_ID")
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = MemeImpl.class)
    @JoinColumn(name = "MEME_ID")
    private Meme meme;
    @Column(name = "TYPE", nullable = false)
    private VoteType type;

    public VoteImpl() {
    }

    public VoteImpl(@NotNull final User user, @NotNull final Meme meme, @NotNull final VoteType type) {
        this.user = user;
        this.meme = meme;
        this.type = type;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setUser(final User user) {
        this.user = user;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setMeme(final Meme meme) {
        this.meme = meme;
    }

    @Override
    public Meme getMeme() {
        return meme;
    }

    @Override
    public void setType(@NotNull final VoteType type) {
        this.type = type;
    }

    @Override
    public VoteType getType() {
        return type;
    }
}
