package com.example.memex.persistence.jpa;

import com.example.memex.api.user.User;
import com.example.memex.api.user.UserDao;
import com.example.memex.persistence.model.user.UserImpl;
import com.example.memex.persistence.model.user.UserImpl_;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class UserJpaDao implements UserDao {
    private static final Logger log = LogManager.getLogger(UserJpaDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User save(@NotNull String username, @NotNull String password) {
        final User user = new UserImpl(username, password);
        save(user);

        return user;
    }

    @Override
    public void save(@NotNull final User user) {
        entityManager.persist(user);
    }

    @Override
    @Nullable
    public User findUser(@NotNull final String username) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        final CriteriaQuery<UserImpl> c = cb.createQuery(UserImpl.class);
        final Root<UserImpl> user = c.from(UserImpl.class);
        c.select(user)
                .where(cb.equal(user.get(UserImpl_.USERNAME), username));

        try {
            return entityManager.createQuery(c).getSingleResult();
        } catch (NoResultException e) {
            log.trace(e);
            return null;
        }
    }

    @Override
    @Nullable
    public User findUserById(final long id) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        final CriteriaQuery<UserImpl> c = cb.createQuery(UserImpl.class);
        final Root<UserImpl> user = c.from(UserImpl.class);
        c.select(user)
                .where(cb.equal(user.get(UserImpl_.ID), id));

        try {
            return entityManager.createQuery(c).getSingleResult();
        } catch (NoResultException e) {
            log.trace(e);
            return null;
        }
    }

    public void deleteAll() {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaDelete<UserImpl> c = cb.createCriteriaDelete(UserImpl.class);
        final Root<UserImpl> ignored = c.from(UserImpl.class);

        try {
            entityManager.createQuery(c).executeUpdate();
        } catch (NoResultException e) {
            log.trace(e);
        }
    }
}
