package com.example.memex.persistence.model.user;

import com.example.memex.api.user.User;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "APP_USER",
        indexes = {
            @Index(name = "APP_USER_NAME_INDEX", columnList = "USERNAME")
        })
public class UserImpl implements User {
    @Id
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "USERNAME", nullable = false, unique = true)
    private String username;
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    public UserImpl() {
    }

    public UserImpl(@NotNull final String username, @NotNull final String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    @NotNull
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(@NotNull final String username) {
        this.username = username;
    }

    @Override
    @NotNull
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(@NotNull final String password) {
        this.password = password;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserImpl user = (UserImpl) o;
        return Objects.equals(id, user.id) && Objects.equals(username, user.username) && Objects.equals(password,
                                                                                                        user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password);
    }
}
