package com.example.memex.api.plugin;

import org.jetbrains.annotations.NotNull;

/**
 * Plugin point for modifying a meme's title
 */
public interface TitleFilter {
    /**
     * Modify meme's title
     * @param title original title
     * @return modified title
     */
    @NotNull
    String filterTitle(@NotNull String title);
}
