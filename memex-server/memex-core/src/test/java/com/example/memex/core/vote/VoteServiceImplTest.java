package com.example.memex.core.vote;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.meme.MemeDao;
import com.example.memex.api.transaction.MemexTransactionManager;
import com.example.memex.api.user.User;
import com.example.memex.api.user.UserDao;
import com.example.memex.api.vote.VoteDao;
import com.example.memex.api.vote.VoteType;
import com.example.memex.core.utils.FakeMeme;
import com.example.memex.core.utils.FakeUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VoteServiceImplTest {
    @Mock
    private MemeDao memeDao;
    @Mock
    private UserDao userDao;
    @Mock
    private VoteDao voteDao;
    @Mock
    private MemexTransactionManager memexTransactionManager;
    @InjectMocks
    private VoteServiceImpl voteService;

    @Captor
    private ArgumentCaptor<User> userArgCaptor;
    @Captor
    private ArgumentCaptor<Meme> memeArgCaptor;
    @Captor
    private ArgumentCaptor<VoteType> voteTypeArgCaptor;

    @BeforeEach
    void init() {
        doAnswer(invocation -> {
            final Runnable callback = invocation.getArgument(0);
            callback.run();
            return null;
        }).when(memexTransactionManager).execute(any(Runnable.class));
    }

    @Test
    void testVoteHappyPath() {
        //given
        final User user = spy(FakeUser.class);
        when(userDao.findUser(anyString())).thenReturn(user);

        final Meme meme = spy(FakeMeme.class);
        when(memeDao.findById(anyLong())).thenReturn(meme);

        //when
        voteService.vote(1L, "user", VoteType.UPVOTE);

        //then
        verify(voteDao, times(1)).save(userArgCaptor.capture(), memeArgCaptor.capture(), voteTypeArgCaptor.capture());
        assertAll(
                () -> assertEquals(userArgCaptor.getValue(), user),
                () -> assertEquals(memeArgCaptor.getValue(), meme),
                () -> assertEquals(voteTypeArgCaptor.getValue(), VoteType.UPVOTE)
        );
    }

    @Test
    void testVoteWithMissingUser() {
        final Exception exception = assertThrows(RuntimeException.class, () ->
                voteService.vote( 123L, "asdf", VoteType.UPVOTE));
        assertTrue(exception.getMessage().contains("asdf"));
        verify(voteDao, never()).save(any(), any(), any());
    }
}