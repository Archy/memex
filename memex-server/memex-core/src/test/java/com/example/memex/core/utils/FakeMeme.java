package com.example.memex.core.utils;

import com.example.memex.api.meme.Meme;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class FakeMeme implements Meme {
    private long id;
    private String title;
    private String fileName;
    private String accessToken;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public @NotNull String getTitle() {
        return title;
    }

    @Override
    public @NotNull String getFileName() {
        return fileName;
    }

    @Override
    public boolean isVisible() {
        return false;
    }

    @Override
    public @Nullable String getAccessToken() {
        return accessToken;
    }

    public void setId(final long id) {
        this.id = id;
    }

    @Override
    public void setTitle(@NotNull String title) {
        this.title = title;
    }

    @Override
    public void setFileName(@NotNull String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void setAccessToken(@Nullable String accessToken) {
        this.accessToken = accessToken;
    }
}