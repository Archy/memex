package com.example.memex.core.meme;

import com.example.memex.api.locks.ScopedExclusionService;
import com.example.memex.api.meme.ImageManager;
import com.example.memex.api.meme.ImmutableMeme;
import com.example.memex.api.meme.MemeAccessService;
import com.example.memex.api.meme.MemeDao;
import com.example.memex.api.meme.cache.CacheInvalidator;
import com.example.memex.api.meme.processing.MemeProcessingService;
import com.example.memex.api.plugin.TitleFilter;
import com.example.memex.api.transaction.MemexTransactionManager;
import com.example.memex.core.locks.LocalScopedExclusionService;
import com.example.memex.core.osgi.PluginManagerImpl;
import com.example.memex.core.utils.FakeMeme;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.InputStream;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MemeServiceImplTest {
    @Mock
    private CacheInvalidator cacheInvalidator;
    @Mock
    private ImageManager imageManager;
    @Mock
    private MemeAccessService memeAccessService;
    @Mock
    private MemeDao memeDao;
    @Mock
    private MemeProcessingService memeProcessingService;
    @Mock
    private MemexTransactionManager memexTransactionManager;
    @Spy
    private FakePluginManager pluginManager;
    @Spy
    private ScopedExclusionService<String> scopedExclusionService = new LocalScopedExclusionService<>();
    @InjectMocks
    private MemeServiceImpl memeService;


    @BeforeEach
    void init() {
        doAnswer(invocation -> {
            final Supplier<?> callback = invocation.getArgument(0);
            return callback.get();
        }).when(memexTransactionManager).execute(any(Supplier.class));

        doAnswer(invocation -> {
            final FakeMeme meme = spy(FakeMeme.class);
            meme.setId(1L);
            meme.setTitle(invocation.getArgument(0));
            meme.setFileName(invocation.getArgument(1));
            meme.setAccessToken(invocation.getArgument(2));

            return meme;
        }).when(memeDao).save(any(), any(), any());
    }

    @Test
    void testMemeSaveHappyPath() {
        //given
        final String title = "abc";
        final InputStream is = mock(InputStream.class);

        when(memeDao.findAll()).thenReturn(Collections.emptyList());

        //when
        final ImmutableMeme meme = memeService.save(title, is);

        //then
        verify(memeDao, times(1)).save(any(), any(), any());
        verify(imageManager, times(1)).saveMeme(any(), any());
        assertAll(
                () -> assertEquals(meme.getTitle(), title + FakePluginManager.SUFIX),
                () -> assertTrue(StringUtils.isNotBlank(meme.getFileName()))
        );
    }

    static abstract class FakePluginManager extends PluginManagerImpl {
        private static final String SUFIX = "_modified";

        private final Map<Class<?>, List<Object>> services = new HashMap<>();

        public FakePluginManager() {
            final TitleFilter titleFilter = mock(TitleFilter.class);
            when(titleFilter.filterTitle(anyString())).thenAnswer(invocation -> {
                final String title = invocation.getArgument(0);
                return title + SUFIX;
            });

            Class<?> clazz = TitleFilter.class;
            services.putIfAbsent(clazz, new ArrayList<>());
            services.get(clazz).add(titleFilter);
        }

        @Override
        public <T> List<T> getServices(final Class<T> clazz) {
            return services.getOrDefault(clazz, Collections.emptyList()).stream()
                    .map(clazz::cast)
                    .collect(Collectors.toList());
        }
    }
}