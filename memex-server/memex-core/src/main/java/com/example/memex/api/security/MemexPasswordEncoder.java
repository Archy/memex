package com.example.memex.api.security;

public interface MemexPasswordEncoder {
    String encodePassword(String password);
}
