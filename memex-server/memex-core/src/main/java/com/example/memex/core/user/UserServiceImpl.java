package com.example.memex.core.user;

import com.example.memex.api.security.MemexPasswordEncoder;
import com.example.memex.api.transaction.MemexTransactionManager;
import com.example.memex.api.user.ImmutableUser;
import com.example.memex.api.user.UserDao;
import com.example.memex.api.user.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserServiceImpl implements UserService {
    private static final Logger log = LogManager.getLogger(UserServiceImpl.class);

    private final MemexPasswordEncoder passwordEncoder;
    private final MemexTransactionManager memexTransactionManager;
    private final UserDao userDao;

    public UserServiceImpl(final MemexPasswordEncoder passwordEncoder,
                           final MemexTransactionManager memexTransactionManager,
                           final UserDao userDao) {
        this.passwordEncoder = passwordEncoder;
        this.memexTransactionManager = memexTransactionManager;
        this.userDao = userDao;
    }

    @Override
    @Nullable
    public ImmutableUser findUser(@NotNull final String username) {
        return memexTransactionManager.execute(() -> userDao.findUser(username));
    }

    @Override
    public ImmutableUser save(@NotNull final String username, @NotNull final String password) {
        log.info("Creating user " + username);
        return memexTransactionManager.execute(() -> userDao.save(username, passwordEncoder.encodePassword(password)));
    }
}
