package com.example.memex.api.meme;

import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.Optional;

public interface ImageManager {
    /**
     * Initialize directory for storing memes' images
     */
    void initialize();

    /**
     * Save meme in file system
     * @param inputStream image source stream
     * @param filename name of image file
     */
    void saveMeme(@NotNull InputStream inputStream, @NotNull String filename);

    /**
     * Get meme image file
     * @param filename meme image name
     */
    Optional<Path> getMeme(@NotNull String filename);
}
