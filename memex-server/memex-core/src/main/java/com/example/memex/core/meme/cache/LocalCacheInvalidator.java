package com.example.memex.core.meme.cache;

import com.example.memex.api.meme.CachedMemeService;
import com.example.memex.api.meme.cache.CacheInvalidator;

public class LocalCacheInvalidator implements CacheInvalidator {
    private final CachedMemeService cachedMemeService;

    public LocalCacheInvalidator(final CachedMemeService cachedMemeService) {
        this.cachedMemeService = cachedMemeService;
    }

    @Override
    public void invalidate(long memeId) {
        cachedMemeService.indexMeme(memeId);
    }
}
