package com.example.memex.api.meme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Mutable representation of a meme
 */
public interface Meme extends ImmutableMeme {
    void setTitle(@NotNull String title);

    void setFileName(@NotNull String fileName);

    void setVisible(boolean visible);

    void setAccessToken(@Nullable String accessToken);

    void makeVisible();
}
