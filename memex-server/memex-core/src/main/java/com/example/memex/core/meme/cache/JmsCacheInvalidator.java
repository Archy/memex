package com.example.memex.core.meme.cache;

import com.example.memex.api.meme.CachedMemeService;
import com.example.memex.api.meme.cache.CacheInvalidator;
import com.example.memex.shared.api.MemeXJMS;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Message;

public class JmsCacheInvalidator implements CacheInvalidator {
    private final CachedMemeService cachedMemeService;
    private final JmsTemplate cacheInvalidationJmsTemplate;
    private final MemeXJMS memeXJMS;

    public JmsCacheInvalidator(final CachedMemeService cachedMemeService,
                               final JmsTemplate cacheInvalidationJmsTemplate,
                               final MemeXJMS memeXJMS) {
        this.cachedMemeService = cachedMemeService;
        this.cacheInvalidationJmsTemplate = cacheInvalidationJmsTemplate;
        this.memeXJMS = memeXJMS;
    }

    @Override
    public void invalidate(final long memeId) {
        cachedMemeService.indexMeme(memeId);

        cacheInvalidationJmsTemplate.send(session -> {
            final Message message = session.createMessage();
            memeXJMS.addMemeIdToMessage(memeId, message);
            memeXJMS.addClientId(message);

            return message;
        });
    }
}
