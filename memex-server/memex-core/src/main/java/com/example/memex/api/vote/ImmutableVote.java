package com.example.memex.api.vote;

import com.example.memex.api.meme.ImmutableMeme;
import com.example.memex.api.user.ImmutableUser;
import org.jetbrains.annotations.Nullable;

public interface ImmutableVote {
    /**
     * Get vote unique id
     */
    long getId();

    /**
     * Get user who has casted the vote
     */
    @Nullable
    ImmutableUser getUser();

    /**
     * Get the vote's meme
     */
    @Nullable
    ImmutableMeme getMeme();

    /**
     * Get vote type
     */
    VoteType getType();
}
