package com.example.memex.core.meme.cache;

import com.example.memex.api.meme.CachedMemeService;
import com.example.memex.api.meme.ImmutableMeme;
import com.example.memex.api.meme.MemeAccessService;
import com.google.common.base.Functions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CachedMemeServiceImpl implements CachedMemeService {
    private static final Logger log = LogManager.getLogger(CachedMemeServiceImpl.class);

    private final LoadingCache<Long, ImmutableMeme> memeCache;
    private final MemeAccessService memeAccessService;

    public CachedMemeServiceImpl(final MemeAccessService memeAccessService) {
        this.memeAccessService = memeAccessService;
        this.memeCache = CacheBuilder.newBuilder()
                .build(new CacheLoader<>() {
                    private MemeAccessService memeService = memeAccessService;

                    @Override
                    @NotNull
                    public ImmutableMeme load(@NotNull final Long memeId) {
                        final ImmutableMeme meme = memeService.findById(memeId);
                        if (meme == null) {
                            throw new RuntimeException("Unable to find meme " + memeId);
                        }

                        return meme;
                    }
                });
    }

    @Override
    public void initialize() {
        log.info("Loading memes to cache");
        // TODO jan: this has to complete before cache invalidation messages comes
        memeCache.putAll(
                memeAccessService.findAll().stream()
                        .collect(Collectors.toMap(ImmutableMeme::getId, Functions.identity()))
        );
        log.info("Memes loaded to cache");
    }

    @Override
    @NotNull
    public List<ImmutableMeme> findAll() {
        return new ArrayList<>(memeCache.asMap().values());
    }

    @Override
    public @Nullable ImmutableMeme findById(final long id) {
        return memeCache.getUnchecked(id);
    }

    @Override
    @Nullable
    public ImmutableMeme findByFilename(@NotNull final String filename) {
        // TODO cache index
        return memeCache.asMap().values().stream()
                .filter(immutableMeme -> immutableMeme.getFileName().equals(filename))
                .findFirst().orElse(null);
    }

    @Override
    public void indexMeme(long memeId) {
        memeCache.refresh(memeId);
    }
}
