package com.example.memex.core.meme;

import com.example.memex.api.meme.ImmutableMeme;
import com.example.memex.api.meme.MemeAccessService;
import com.example.memex.api.meme.MemeDao;
import com.example.memex.api.transaction.MemexTransactionManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class MemeAccessServiceImpl implements MemeAccessService {
    private final MemeDao memeDao;
    private final MemexTransactionManager memexTransactionManager;

    public MemeAccessServiceImpl(final MemeDao memeDao, final MemexTransactionManager memexTransactionManager) {
        this.memeDao = memeDao;
        this.memexTransactionManager = memexTransactionManager;
    }

    @Override
    public @NotNull List<ImmutableMeme> findAll() {
        return memexTransactionManager.execute(() -> new ArrayList<>(memeDao.findAll()));
    }

    @Override
    public @Nullable ImmutableMeme findById(long id) {
        return memexTransactionManager.execute(() -> memeDao.findById(id));
    }
}
