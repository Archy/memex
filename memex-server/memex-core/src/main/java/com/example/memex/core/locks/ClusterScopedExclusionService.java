package com.example.memex.core.locks;

import com.example.memex.api.cluster.MemeXNode;
import com.example.memex.api.locks.ScopedExclusionService;
import com.example.memex.core.meme.MemeServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ClusterScopedExclusionService implements ScopedExclusionService<String> {
    private static final Logger log = LogManager.getLogger(MemeServiceImpl.class);

    private final MemeXNode memeXNode;
    private final Map<String, WeakReference<? extends Lock>> locks = new ConcurrentHashMap<>();

    public ClusterScopedExclusionService(MemeXNode memeXNode) {
        this.memeXNode = memeXNode;
    }


    @Override
    public <V> V doWithUniquenessAssurance(final Supplier<String> supplier,
                                           final Predicate<String> uniquenessPredicated,
                                           final Function<String, V> consumer) {
        for (int i = 0; i < MAX_ATTEMPTS; ++i) {
            log.debug(String.format("Attempt %d/%d", i, MAX_ATTEMPTS));



            final String uniqueString = supplier.get();
            if (uniquenessPredicated.test(uniqueString)) {
                final Lock lock = locks
                        .computeIfAbsent(uniqueString, key -> new WeakReference<>(memeXNode.getHzInstance().getCPSubsystem().getLock(key)))
                        .get();

                lock.lock();
                try {
                    if (uniquenessPredicated.test(uniqueString)) {
                        return consumer.apply(uniqueString);
                    }
                } finally {
                    lock.unlock();
                }
            } else {
                log.debug("Failed to generate unique filename; Retrying");
            }
        }
        throw new RuntimeException("Failed to create unique object");
    }
}
