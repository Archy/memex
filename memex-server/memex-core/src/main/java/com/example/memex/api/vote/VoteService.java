package com.example.memex.api.vote;

import org.jetbrains.annotations.NotNull;

/**
 * Service for managing users votes on memes
 */
public interface VoteService {
    /**
     * Get votes score for a given meme
     */
    long countVotes(long memeId);

    /**
     * Add user's vote on a meme
     */
    void vote(long memeId, @NotNull String username, @NotNull VoteType voteType);

    /**
     * Remove a user's vote from a given meme
     */
    void removeVote(long memeId, @NotNull String username);
}
