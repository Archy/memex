package com.example.memex.api.properties;

public interface MemeXProperties {
    static boolean isFreshInit() {
        return Boolean.getBoolean("memex.fresh.init");
    }
}
