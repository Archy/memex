package com.example.memex.api.meme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface MemeDao {
    /**
     * Creates and save a new meme
     */
    Meme save(@NotNull final String title, @NotNull final String fileName, @NotNull final String accessToken);

    /**
     * Updates a meme
     */
    void update(@NotNull Meme meme);

    /**
     * Returns list of all memes
     */
    @NotNull
    List<Meme> findAll();

    /**
     * Finds meme by id
     */
    @Nullable
    Meme findById(final long id);
}
