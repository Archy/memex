package com.example.memex.api.osgi;

/**
 * Starts and manages bundled OSGi framework container
 */
public interface OsgiContainerManager {
    /**
     * Start embedded OSGi container
     */
    void intializeOsgiContainer();

    /**
     * Stop embedded OSGi container
     */
    void stopOsgiContainer();
}
