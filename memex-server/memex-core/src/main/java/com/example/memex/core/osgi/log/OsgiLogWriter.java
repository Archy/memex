package com.example.memex.core.osgi.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogLevel;
import org.osgi.service.log.LogListener;

public class OsgiLogWriter implements LogListener {
    private static final Logger log = LogManager.getLogger(OsgiLogWriter.class);

    @Override
    public void logged(LogEntry logEntry) {
            log.log(osgi2log4j(logEntry.getLogLevel()), logEntry.getMessage());
    }

    private Level osgi2log4j(final LogLevel logLevel) {
        return switch (logLevel) {
            case AUDIT, INFO -> Level.INFO;
            case WARN -> Level.WARN;
            case DEBUG -> Level.DEBUG;
            case TRACE -> Level.TRACE;
            case ERROR -> Level.ERROR;
        };
    }
}
