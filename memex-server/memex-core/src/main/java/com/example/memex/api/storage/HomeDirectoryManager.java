package com.example.memex.api.storage;

import java.nio.file.Path;

/**
 * Manager for MemeX home directory
 */
public interface HomeDirectoryManager {

    /**
     * Initialize home dir
     */
    void initialize();

    /**
     * @return memex cluster shared home directory path
     */
    Path getHomeDirectory();

    /**
     * @return node's local home directory path
     */
    Path getLocalHomeDirectory();
}
