package com.example.memex.core.meme;

import com.example.memex.api.locks.ScopedExclusionService;
import com.example.memex.api.meme.*;
import com.example.memex.api.meme.cache.CacheInvalidator;
import com.example.memex.api.meme.processing.MemeProcessingService;
import com.example.memex.api.osgi.PluginManager;
import com.example.memex.api.plugin.TitleFilter;
import com.example.memex.api.transaction.MemexTransactionManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

public class MemeServiceImpl implements MemeService {
    private static final Logger log = LogManager.getLogger(MemeServiceImpl.class);

    private final CacheInvalidator cacheInvalidator;
    private final ImageManager imageManager;
    private final MemeAccessService memeAccessService;
    private final MemeDao memeDao;
    private final MemeProcessingService memeProcessingService;
    private final MemexTransactionManager memexTransactionManager;
    private final PluginManager pluginManager;
    private final ScopedExclusionService<String> scopedExclusionService;

    public MemeServiceImpl(final CacheInvalidator cacheInvalidator,
                           final ImageManager imageManager,
                           final MemeAccessService memeAccessService,
                           final MemeDao memeDao,
                           final MemeProcessingService memeProcessingService,
                           final MemexTransactionManager memexTransactionManager,
                           final PluginManager pluginManager,
                           final ScopedExclusionService<String> scopedExclusionService) {
        this.cacheInvalidator = cacheInvalidator;
        this.imageManager = imageManager;
        this.memeAccessService = memeAccessService;
        this.memeDao = memeDao;
        this.memeProcessingService = memeProcessingService;
        this.memexTransactionManager = memexTransactionManager;
        this.pluginManager = pluginManager;
        this.scopedExclusionService = scopedExclusionService;
    }

    @Override
    @NotNull
    public List<ImmutableMeme> findAll() {
        return memeAccessService.findAll();
    }

    @Override
    @Nullable
    public ImmutableMeme findById(final long id) {
        return memeAccessService.findById(id);
    }

    @Override
    public ImmutableMeme save(final @NotNull String originalTitle, @NotNull final InputStream inputStream) {
        log.info("Saving meme: " + originalTitle);
        final String title = modifyTitle(originalTitle);

        final Meme meme = memexTransactionManager.execute(() ->
                scopedExclusionService.doWithUniquenessAssurance(
                    () -> String.format("%s.%s", RandomStringUtils.randomAlphanumeric(8), "jpg"),
                    filename -> memeDao.findAll().stream().map(Meme::getFileName).noneMatch(f -> f.equals(filename)),
                    filename -> scopedExclusionService.doWithUniquenessAssurance(
                            () -> RandomStringUtils.randomAlphanumeric(8),
                            accessToken -> memeDao.findAll().stream()
                                    .map(Meme::getAccessToken)
                                    .filter(Objects::nonNull)
                                    .noneMatch(f -> f.equals(accessToken)),
                            accessToken -> {
                                // if exception is thrown after file is saved (eg. during transaction commit) then the file won't be cleaned up
                                // FIXME: write orphaned file removal job
                                final Meme memeEntity = memeDao.save(title, filename, accessToken);
                                imageManager.saveMeme(inputStream, memeEntity.getFileName());
                                return memeEntity;
                            })
            )
        );

        cacheInvalidator.invalidate(meme.getId());
        memeProcessingService.enqueueMeme(meme);

        return meme;
    }

    @Override
    public void updateMeme(final long memeId, final byte[] memeBytes) {
        memexTransactionManager.execute(() -> {
            final InputStream inputStream = new ByteArrayInputStream(memeBytes);
            final Meme memeEntity = memeDao.findById(memeId);

            imageManager.saveMeme(inputStream, memeEntity.getFileName());
            memeEntity.makeVisible();
            memeDao.update(memeEntity);
        });
        cacheInvalidator.invalidate(memeId);
    }

    private String modifyTitle(@NotNull String title) {
        for (TitleFilter titleFilter: pluginManager.getServices(TitleFilter.class)) {
            title = titleFilter.filterTitle(title);
            log.debug(String.format("Service %s modifies title to %s", titleFilter, title));
        }
        log.info("Final meme title is: " + title);
        return title;
    }
}
