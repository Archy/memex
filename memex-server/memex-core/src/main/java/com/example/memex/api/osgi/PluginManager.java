package com.example.memex.api.osgi;

import java.util.List;

/**
 * Manager for handling plugins and their exposed OSGi services
 */
public interface PluginManager {
    /**
     * Get all services for given plugin point.
     * All plugins point are listed under {@link com.example.memex.api.plugin}
     */
    <T> List<T> getServices(Class<T> clazz);

    /**
     * Register plugin's OSGi service. Internal use only
     */
    void registerService(Class<?> clazz, Object service);

    /**
     * Unregister plugin's OSGi service. Internal use only
     */
    void unregisterService(Class<?> clazz, Object service);
}
