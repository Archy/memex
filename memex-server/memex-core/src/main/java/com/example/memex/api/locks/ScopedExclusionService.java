package com.example.memex.api.locks;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public interface ScopedExclusionService<T> {
    int MAX_ATTEMPTS = 10;

    /**
     * Create object and use it with assurance that it stays unique through whole operation
     *
     * @param supplier creates an object that's suppose to be unique
     * @param uniquenessPredicated tests if the created object is unique. Should return true if the object is unique
     * @param consumer consumer of the created object. Has assurance that the object is and stays unique
     */
    <V> V doWithUniquenessAssurance(final Supplier<T> supplier,
                                    final Predicate<T> uniquenessPredicated,
                                    final Function<T, V> consumer);
}
