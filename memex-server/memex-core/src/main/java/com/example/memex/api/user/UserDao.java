package com.example.memex.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface UserDao {
    /**
     * Creates and saves a user
     */
    User save(@NotNull final String username, @NotNull final String password);

    /**
     * Saves a user
     */
    void save(@NotNull User user);

    /**
     * Find user by username
     */
    @Nullable
    User findUser(@NotNull String username);

    /**
     * Find user by id
     */
    @Nullable
    User findUserById(long id);
}
