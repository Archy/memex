package com.example.memex.api.meme.cache;

public interface CacheInvalidator {
    /**
     * Invalidate meme in cache
     */
    void invalidate(long memeId);
}
