package com.example.memex.api.user;

import org.jetbrains.annotations.NotNull;

/**
 * An immutable representation of a user
 */
public interface ImmutableUser {
    /**
     * Get user unique id
     */
    long getId();

    /**
     * Get user name
     */
    @NotNull String getUsername();

    /**
     * Get user hashed password
     */
    @NotNull String getPassword();
}
