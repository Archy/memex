package com.example.memex.api.user;

import org.jetbrains.annotations.NotNull;

/**
 * Mutable representation of a user
 */
public interface User extends ImmutableUser {
    void setUsername(@NotNull final String username);

    void setPassword(@NotNull final String password);
}
