package com.example.memex.api.vote;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.user.User;
import org.jetbrains.annotations.NotNull;

public interface VoteDao {
    /**
     * Get votes score for a given meme
     */
    long countVotes(long memeId);

    /**
     * Creates and saves a vote
     */
    Vote save(@NotNull final User user, @NotNull final Meme meme, @NotNull final VoteType voteType);

    /**
     * Saves a vote
     */
    void save(@NotNull Vote vote);

    /**
     * Checks if vote was already casted by the user
     */
    boolean isVoteAlreadyCasted(@NotNull User user, @NotNull Meme meme);

    /**
     * Remove a user's vote from a given meme
     */
    void removeVote(@NotNull final User user, @NotNull final Meme meme);
}
