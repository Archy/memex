package com.example.memex.core.meme.cache;

import com.example.memex.api.meme.CachedMemeService;
import com.example.memex.shared.api.MemeXJMS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;

import javax.jms.Message;


public class CacheInvalidationListener {
    private static final Logger log = LogManager.getLogger(CacheInvalidationListener.class);

    private final CachedMemeService cachedMemeService;
    private final MemeXJMS memeXJMS;

    public CacheInvalidationListener(final CachedMemeService cachedMemeService,
                                     final MemeXJMS memeXJMS) {
        this.cachedMemeService = cachedMemeService;
        this.memeXJMS = memeXJMS;
    }

    @JmsListener(
            destination = MemeXJMS.CACHE_SYNC_TOPIC,
            subscription = MemeXJMS.CACHE_SYNC_TOPIC,
            containerFactory = "cacheInvalidationJmsListenerContainerFactory"
    )
    public void receive(final Message message) {
        if (memeXJMS.isOwnMessage(message)) {
            log.debug("Received own cache invalidation message. Skipping processing");
            return;
        }

        final long memeId = memeXJMS.getMemeIdFromMessage(message);
        log.debug("Received cache invalidation message for " + memeId);

        cachedMemeService.indexMeme(memeId);
    }
}
