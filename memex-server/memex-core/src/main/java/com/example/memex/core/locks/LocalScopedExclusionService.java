package com.example.memex.core.locks;

import com.example.memex.api.locks.ScopedExclusionService;
import com.example.memex.core.meme.MemeServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LocalScopedExclusionService<T> implements ScopedExclusionService<T> {
    private static final Logger log = LogManager.getLogger(MemeServiceImpl.class);

    private final Map<T, WeakReference<? extends Lock>> locks = new ConcurrentHashMap<>();


    @Override
    public <V> V doWithUniquenessAssurance(final Supplier<T> supplier,
                                    final Predicate<T> uniquenessPredicated,
                                    final Function<T, V> consumer) {
        for (int i = 0; i < MAX_ATTEMPTS; ++i) {
            log.debug(String.format("Attempt %d/%d", i, MAX_ATTEMPTS));

            final T t = supplier.get();
            if (uniquenessPredicated.test(t)) {
                final Lock lock = locks
                        .computeIfAbsent(t, ignored -> new WeakReference<>(new ReentrantLock()))
                        .get();
                lock.lock();
                try {
                    if (uniquenessPredicated.test(t)) {
                        return consumer.apply(t);
                    }
                } finally {
                    lock.unlock();
                }
            } else {
                log.debug("Failed to generate unique filename; Retrying");
            }
        }
        throw new RuntimeException("Failed to create unique object");
    }
}
