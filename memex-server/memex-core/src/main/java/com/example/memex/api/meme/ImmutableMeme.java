package com.example.memex.api.meme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * An immutable representation of a meme
 */
public interface ImmutableMeme {
    /**
     * Get meme unique id
     */
    long getId();

    /**
     * Get meme's title
     */
    @NotNull
    String getTitle();

    /**
     * Get meme's image file name
     */
    @NotNull
    String getFileName();

    /**
     * Whether meme is ready to be shown to users
     */
    boolean isVisible();

    /**
     * Get token used for accessing hidden memes
     */
    @Nullable
    String getAccessToken();
}
