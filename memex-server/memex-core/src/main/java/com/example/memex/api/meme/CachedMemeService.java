package com.example.memex.api.meme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Service for managing memes and memes cache
 */
public interface CachedMemeService extends MemeAccessService {
    /**
     * Load existing memes into cache
     */
    void initialize();

    /**
     * Finds meme by its filename
     */
    @Nullable
    ImmutableMeme findByFilename(@NotNull String filename);

    /**
     * Indexes meme
     */
    void indexMeme(long memeId);
}
