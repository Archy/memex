package com.example.memex.core.cluster;

import com.example.memex.api.cluster.MemeXNode;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class MemeXHzNode implements MemeXNode {
    private static final Logger log = LogManager.getLogger(MemeXHzNode.class);

    private HazelcastInstance hzInstance;

    public void initialize() {
        log.info("Starting hazelcast");
        hzInstance = Hazelcast.newHazelcastInstance();
        log.info("Hazelcast started");
    }

    public void stop() {
        if (hzInstance != null) {
            hzInstance.shutdown();
        }
    }

    @Override
    @NotNull
    public HazelcastInstance getHzInstance() {
        if (hzInstance == null) {
            throw new RuntimeException("Hazelcast is not initialized");
        }

        return hzInstance;
    }

}
