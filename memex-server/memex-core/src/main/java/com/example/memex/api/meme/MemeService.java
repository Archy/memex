package com.example.memex.api.meme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.List;

/**
 * Service for managing memes
 */
public interface MemeService extends MemeAccessService {
    /**
     * Save meme
     * @param title meme title
     * @param inputStream meme's image input stream
     */
    ImmutableMeme save(@NotNull String title, @NotNull InputStream inputStream);

    /**
     * Update meme image and make it visible
     */
    void updateMeme(long memeId, byte[] memeBytes);
}
