package com.example.memex.api.cluster;

import com.hazelcast.core.HazelcastInstance;
import org.jetbrains.annotations.NotNull;

public interface MemeXNode {
    @NotNull
    HazelcastInstance getHzInstance();
}
