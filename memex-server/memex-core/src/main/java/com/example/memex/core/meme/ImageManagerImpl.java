package com.example.memex.core.meme;

import com.example.memex.api.meme.ImageManager;
import com.example.memex.api.properties.MemeXProperties;
import com.example.memex.api.storage.HomeDirectoryManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;

public class ImageManagerImpl implements ImageManager {
    private static final Logger log = LogManager.getLogger(ImageManagerImpl.class);

    private static final String MEMES_DIR = "memes";
    private final Path directory;

    public ImageManagerImpl(HomeDirectoryManager homeDirectoryManager) {
        this.directory =  homeDirectoryManager.getHomeDirectory().resolve(MEMES_DIR);
    }

    @Override
    public void initialize() {
        if (MemeXProperties.isFreshInit()) {
            if (directory.toFile().exists()) {
                deleteOldDirectory();
            }
        }

        if (!directory.toFile().exists()) {
            if (!directory.toFile().mkdir()) {
                throw new IllegalStateException("Failed to create memes directory");
            }
        }

        log.info(String.format("Memes are stored in: %s", directory));
    }

    @Override
    public void saveMeme(@NotNull final InputStream inputStream, @NotNull final String filename) {
        try (final var outputStream = new FileOutputStream(Paths.get(directory.toString(), filename).toFile())) {
            int read;
            final byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            log.info(String.format("Image '%s' saved", filename));
        } catch (IOException e) {
            throw new RuntimeException("Failed to save image", e);
        }
    }

    @Override
    public Optional<Path> getMeme(@NotNull String filename) {
        final Path file = directory.resolve(filename);
        return file.toFile().exists() ? Optional.of(file) : Optional.empty();
    }

    private void deleteOldDirectory() {
        try {
            Files.walk(directory)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to delete old memes directory", e);
        }
    }
}
