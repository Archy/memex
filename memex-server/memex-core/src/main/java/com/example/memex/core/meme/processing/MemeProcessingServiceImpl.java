package com.example.memex.core.meme.processing;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.meme.processing.MemeProcessingService;
import com.example.memex.shared.api.MemeSender;
import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class MemeProcessingServiceImpl implements MemeProcessingService {
    private static final Logger log = LogManager.getLogger(MemeProcessingServiceImpl.class);
    private static final String MEMEX_PORT = "8080";

    private final MemeSender memeSender;
    private final String memexUrl;

    public MemeProcessingServiceImpl(final MemeSender memeSender, final String memexUrl) {
        log.debug("MemeX host is " + memexUrl);
        this.memeSender = memeSender;
        this.memexUrl = memexUrl;
    }

    @Override
    public void enqueueMeme(final Meme meme) {
        try {
            URL url = new URIBuilder()
                    .setScheme("http")
                    .setHost(String.format("%s:%s", memexUrl, MEMEX_PORT))
                    .setPath(String.format("image/%s", meme.getFileName()))
                    .addParameter("token", meme.getAccessToken())
                    .build().toURL();
            memeSender.sendMeme(meme.getId(), url);
        } catch(URISyntaxException | MalformedURLException e) {
            log.error(e);
            throw new RuntimeException(e);
        }


    }

}
