package com.example.memex.core.osgi.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

public class FelixLogger extends org.apache.felix.framework.Logger {
    private static final Logger log = LogManager.getLogger(FelixLogger.class);

    @Override
    protected void doLog(Bundle bundle, ServiceReference sr, int level, String msg, Throwable throwable) {
        final String builder = "[" + bundle.getSymbolicName() + "]: " + msg;
        logInternal(level, builder, throwable);
    }

    @Override
    protected void doLog(int level, String msg, Throwable throwable) {
        logInternal(level, msg, throwable);
    }

    private void logInternal(int level, @NotNull final String msg, @Nullable final Throwable throwable) {
        if (throwable != null) {
            log.log(felixLevel2log4jLevel(level), msg, throwable);
        } else {
            log.log(felixLevel2log4jLevel(level), msg);
        }

    }

    private Level felixLevel2log4jLevel(int level) {
        switch (level) {
            case LOG_ERROR:
                return Level.ERROR;
            case LOG_WARNING:
                return Level.WARN;
            case LOG_INFO:
                return Level.INFO;
            case LOG_DEBUG:
                return Level.DEBUG;
            default:
                log.warn("Unknown log level: " + level);
                return Level.DEBUG;
        }
    }
}
