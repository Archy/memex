package com.example.memex.core.osgi;

import com.example.memex.api.osgi.MemexSystemBundleActivator;
import com.example.memex.api.osgi.OsgiContainerManager;
import com.example.memex.api.storage.HomeDirectoryManager;
import com.example.memex.core.osgi.log.FelixLogger;
import org.apache.felix.framework.Felix;
import org.apache.felix.framework.util.FelixConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;

import java.io.File;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

public class OsgiContainerManagerImpl implements OsgiContainerManager {
    private static final Logger log = LogManager.getLogger(OsgiContainerManagerImpl.class);
    private static final String OSGI_STORAGE_DIR = "osgi_cache";
    private static final String PLUGINS_DIR = "plugins";

    private final HomeDirectoryManager homeDirectoryManager;
    private final MemexSystemBundleActivator hostActivator;

    private Felix felixContainer;

    public OsgiContainerManagerImpl(final HomeDirectoryManager homeDirectoryManager,
                                    final MemexSystemBundleActivator hostActivator) {
        this.homeDirectoryManager = homeDirectoryManager;
        this.hostActivator = hostActivator;
    }

    @Override
    public void intializeOsgiContainer() {
        try {
            startOsgiContainer();
            loadPlugins();
        } catch (BundleException e) {
            log.error(e);
            throw new RuntimeException(e);
        }

        log.info("Listing OSGi registered services");
        for (ServiceReference<?> sr : felixContainer.getRegisteredServices()) {
            log.info(sr.toString());
        }

    }

    @Override
    public void stopOsgiContainer() {
        log.info("OSGi stopping");
        try {
            felixContainer.stop();
            felixContainer.waitForStop(1000);
        } catch (BundleException | InterruptedException e) {
            log.error(e);
            throw new RuntimeException(e);
        }
        log.info("OSGi stopped");
    }

    private void startOsgiContainer() throws BundleException {
        log.info("OSGi starting");

        final Map<String, Object> map = Map.of(
                Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, "com.example.memex.api.plugin,org.apache.logging.log4j;version=2.14.1,org.osgi.framework",
                Constants.FRAMEWORK_STORAGE, homeDirectoryManager.getLocalHomeDirectory().resolve(OSGI_STORAGE_DIR).toAbsolutePath().toString(),
                Constants.FRAMEWORK_STORAGE_CLEAN, Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT,

                FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, Collections.singletonList(hostActivator),
                FelixConstants.LOG_LEVEL_PROP, "999",
                FelixConstants.LOG_LOGGER_PROP, new FelixLogger()
        );
        felixContainer = new Felix(map);
        felixContainer.start();
        hostActivator.setSystemBundle(felixContainer);

        log.info("OSGi started");
    }

    private void loadPlugins() throws BundleException {
        final Path pluginsDir = homeDirectoryManager.getHomeDirectory().resolve(PLUGINS_DIR);
        log.debug("Plugins dir: " + pluginsDir.toFile().getAbsolutePath());
        log.debug("Plugins files: " + String.join(",", pluginsDir.toFile().list()));
        final File[] pluginsFiles = pluginsDir.toFile().listFiles((dir, name) -> name.toLowerCase().endsWith(".jar"));
        if (pluginsFiles == null || pluginsFiles.length == 0) {
            log.error("No plugins found");
            return;
        }

        for (File pluginFile: pluginsFiles) {
            log.info("Loading plugin " + pluginFile.getName());
            final Bundle b = felixContainer.getBundleContext().installBundle("file:" + pluginFile.getAbsolutePath());
            log.info(String.format("Starting bundle %s", b.getSymbolicName()));
            b.start();
        }
    }
}
