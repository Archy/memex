package com.example.memex.core.meme.processing;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.meme.MemeDao;
import com.example.memex.api.meme.cache.CacheInvalidator;
import com.example.memex.api.meme.processing.MemeProcessingService;
import com.example.memex.api.transaction.MemexTransactionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * No-op meme processing service
 */
public class AgentlessMemeProcessingService implements MemeProcessingService {
    private static final Logger log = LogManager.getLogger(AgentlessMemeProcessingService.class);

    private final CacheInvalidator cacheInvalidator;
    private final MemeDao memeDao;
    private final MemexTransactionManager memexTransactionManager;

    private final String activeProfile;

    public AgentlessMemeProcessingService(final CacheInvalidator cacheInvalidator,
                                          final MemeDao memeDao,
                                          final MemexTransactionManager memexTransactionManager,
                                          final String activeProfile) {
        this.cacheInvalidator = cacheInvalidator;
        this.memeDao = memeDao;
        this.memexTransactionManager = memexTransactionManager;
        this.activeProfile = activeProfile;
    }

    @Override
    public void enqueueMeme(final Meme meme) {
        log.debug("Not sending meme since active profile is " + activeProfile);
        memexTransactionManager.execute(() -> {
            final Meme memeEntity = memeDao.findById(meme.getId());
            memeEntity.makeVisible();
            memeDao.update(memeEntity);
        });

        cacheInvalidator.invalidate(meme.getId());
        log.debug(String.format("Meme %d is now visible", meme.getId()));
    }
}
