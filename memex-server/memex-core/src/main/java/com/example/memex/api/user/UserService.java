package com.example.memex.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Service for managing users
 */
public interface UserService {
    /**
     * Find user by their username
     */
    @Nullable
    ImmutableUser findUser(@NotNull String username);

    /**
     * Save user
     * @param username username
     * @param password plain password. Will be hashed before saving in database.
     */
    ImmutableUser save(@NotNull String username, @NotNull String password);
}
