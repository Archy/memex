package com.example.memex.api.vote;

/**
 * Type of a user's vote on a meme
 */
public enum VoteType {
    UPVOTE(1L),
    DOWNVOTE(2L);

    /**
     * ID for storing vote type in database
     */
    final long id;

    VoteType(final long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public static VoteType fromString(final String voteTypeString) {
        for (VoteType voteType: VoteType.values()) {
            if (voteType.name().equalsIgnoreCase(voteTypeString)) {
                return voteType;
            }
        }
        return null;
    }
}
