package com.example.memex.api.vote;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.user.User;

/**
 * Mutable representation of a vote
 */
public interface Vote extends ImmutableVote {
    void setUser(User user);

    void setMeme(Meme meme);

    void setType(VoteType type);
}
