package com.example.memex.core.storage;

import com.example.memex.api.storage.HomeDirectoryManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;


public class HomeDirectoryManagerImpl implements HomeDirectoryManager {
    protected static final Logger log = LogManager.getLogger(HomeDirectoryManagerImpl.class);
    protected final Path homeDirectory;
    protected final Path localHomeDirectory;

    public HomeDirectoryManagerImpl(final String homeDirectory,
                                    final String localHomeDirectory) {
        this.homeDirectory = Paths.get(homeDirectory);
        this.localHomeDirectory = Paths.get(localHomeDirectory);
    }

    @Override
    public void initialize() {
        if (!this.homeDirectory.toFile().exists()) {
            log.debug("Creating home directory");
            if (!homeDirectory.toFile().mkdirs()) {
                final String msg = "Failed to create home directory!";
                log.error(msg);
                throw new RuntimeException(msg);
            }
        }
    }

    @Override
    public Path getHomeDirectory() {
        return homeDirectory;
    }

    @Override
    public Path getLocalHomeDirectory() {
        return localHomeDirectory;
    }
}
