package com.example.memex.core.osgi.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogReaderService;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import java.util.ArrayList;
import java.util.List;


//FIXME: all services should be released
public class OsgiLoggerManager {
    private static final Logger log = LogManager.getLogger(OsgiLoggerManager.class);

    private BundleContext bundleContext;
    private ServiceTracker<LogReaderService, LogReaderService> tracker;
    private final OsgiLogWriter osgiLogWriter = new OsgiLogWriter();
    private final List<LogReaderService> readers = new ArrayList<>();


    public void start(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
        this.tracker = new ServiceTracker<>(bundleContext, LogReaderService.class.getName(), new LogReaderServiceTracker());
        tracker.open();

        final LogReaderService[] logReaderServices = this.tracker.getServices(new LogReaderService[0]);
        if (logReaderServices.length > 0) {
            log.info(String.format("Adding log writer to %d readers", logReaderServices.length));
            for (LogReaderService logReaderService: logReaderServices) {
                log.info("Adding log writer to " + logReaderService);
                logReaderService.addLogListener(osgiLogWriter);
                readers.add(logReaderService);
            }
        } else {
            log.warn("No log reader service found");
        }
    }

    public void stop() {
        log.info("Removing custom log writer from all services");
        readers.forEach(reader -> reader.removeLogListener(osgiLogWriter));
        readers.clear();
        tracker.close();
    }

    private class LogReaderServiceTracker implements ServiceTrackerCustomizer<LogReaderService, LogReaderService> {

        @Override
        public LogReaderService addingService(ServiceReference<LogReaderService> reference) {
            LogReaderService logReaderService = bundleContext.getService(reference);
            log.info("Adding custom log writer to " + logReaderService);
            readers.add(logReaderService);
            logReaderService.addLogListener(osgiLogWriter);

            return logReaderService;
        }

        @Override
        public void modifiedService(ServiceReference<LogReaderService> reference, LogReaderService service) {
            log.error("Handling log service modification is not implemented");
        }

        @Override
        public void removedService(ServiceReference<LogReaderService> reference, LogReaderService service) {
            log.info("Removing custom log writer from " + service);
            service.removeLogListener(osgiLogWriter);
            readers.remove(service);
        }
    }

}
