package com.example.memex.api.transaction;

import java.util.function.Supplier;

public interface MemexTransactionManager {
    <T> T execute(Supplier<T> callback);

    void execute(Runnable callback);
}
