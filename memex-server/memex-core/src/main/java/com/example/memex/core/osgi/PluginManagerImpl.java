package com.example.memex.core.osgi;

import com.example.memex.api.osgi.PluginManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;


public class PluginManagerImpl implements PluginManager {

    private static final Logger log = LogManager.getLogger(PluginManagerImpl.class);
    private final Map<Class<?>, List<Object>> services = new HashMap<>();

    @Override
    public <T> List<T> getServices(Class<T> clazz) {
        return services.getOrDefault(clazz, Collections.emptyList()).stream().map(clazz::cast).collect(Collectors.toList());
    }

    @Override
    public void registerService(Class<?> clazz, Object service) {
        log.debug("Adding service for class " + clazz);
        services.putIfAbsent(clazz, new ArrayList<>());
        services.get(clazz).add(service);
    }

    @Override
    public void unregisterService(Class<?> clazz, Object service) {
        log.debug("Removing service for class " + clazz);

        final List<Object> registeredServices = services.get(clazz);
        registeredServices.remove(service);
        if (registeredServices.isEmpty()) {
            services.remove(clazz);
        }
    }
}
