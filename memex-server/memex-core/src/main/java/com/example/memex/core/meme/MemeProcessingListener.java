package com.example.memex.core.meme;

import com.example.memex.api.meme.MemeService;
import com.example.memex.shared.api.MemeXJMS;
import com.example.memex.shared.api.ProcessedMemeSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;

import javax.jms.JMSException;
import javax.jms.Message;

public class MemeProcessingListener {
    private static final Logger log = LogManager.getLogger(MemeProcessingListener.class);

    private final MemeService memeService;
    private final MemeXJMS memeXJMS;
    private final ProcessedMemeSender processedMemeSender;

    public MemeProcessingListener(final MemeService memeService,
                                  final MemeXJMS memeXJMS,
                                  final ProcessedMemeSender processedMemeSender) {
        this.memeService = memeService;
        this.memeXJMS = memeXJMS;
        this.processedMemeSender = processedMemeSender;
    }

    @JmsListener(destination = MemeXJMS.PROCESSED_MEME_QUEUE)
    void receive(final Message message) throws JMSException {
        final long memeId = memeXJMS.getMemeIdFromMessage(message);
        log.debug(String.format("Received processed meme %d (meessage id: %s", memeId, message.getJMSMessageID()));

        final byte[] bytes = processedMemeSender.getMeme(message);
        memeService.updateMeme(memeId, bytes);

        log.debug(String.format("Meme %d processed", memeId));
    }

}
