package com.example.memex.api.meme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface MemeAccessService {
    /**
     * Get list of all memes
     * @return list of all memes
     */
    @NotNull
    List<ImmutableMeme> findAll();

    /**
     * Finds meme by id. Returns null if meme is not found
     */
    @Nullable
    ImmutableMeme findById(long id);
}
