package com.example.memex.api.meme.processing;

import com.example.memex.api.meme.Meme;

/**
 * Send memes to agents for processing
 */
public interface MemeProcessingService {
    /**
     * Enqueue meme for processing before adding it to public meme
     */
    void enqueueMeme(final Meme meme);
}
