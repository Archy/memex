package com.example.memex.core.vote;

import com.example.memex.api.meme.Meme;
import com.example.memex.api.meme.MemeDao;
import com.example.memex.api.transaction.MemexTransactionManager;
import com.example.memex.api.user.User;
import com.example.memex.api.user.UserDao;
import com.example.memex.api.vote.VoteDao;
import com.example.memex.api.vote.VoteService;
import com.example.memex.api.vote.VoteType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class VoteServiceImpl implements VoteService {
    private static final Logger log = LogManager.getLogger(VoteServiceImpl.class);

    private final MemeDao memeDao;
    private final UserDao userDao;
    private final VoteDao voteDao;
    private final MemexTransactionManager memexTransactionManager;

    public VoteServiceImpl(final MemeDao memeDao,
                           final UserDao userDao,
                           final VoteDao voteDao,
                           final MemexTransactionManager memexTransactionManager) {
        this.memeDao = memeDao;
        this.userDao = userDao;
        this.voteDao = voteDao;
        this.memexTransactionManager = memexTransactionManager;
    }

    @Override
    public long countVotes(final long memeId) {
        return memexTransactionManager.execute(() -> voteDao.countVotes(memeId));
    }

    @Override
    public void vote(final long memeId, @NotNull final String username, @NotNull final VoteType voteType) {
        memexTransactionManager.execute(() -> {
            final User user = getUser(username);
            final Meme meme = getMeme(memeId);

            if (voteDao.isVoteAlreadyCasted(user, meme)) {
                throw new RuntimeException("You've already voted");
            }

            voteDao.save(user, meme, voteType);
        });
    }

    @Override
    public void removeVote(final long memeId, @NotNull final String username) {
        memexTransactionManager.execute(() -> {
            final User user = getUser(username);
            final Meme meme = getMeme(memeId);

            voteDao.removeVote(user, meme);
        });
    }

    @NotNull
    private Meme getMeme(final long memeId) {
        final Meme meme = memeDao.findById(memeId);
        if (meme == null) {
            throw new RuntimeException(String.format("Meme with id %d not found", memeId));
        }
        return meme;
    }

    @NotNull
    private User getUser(@NotNull final String username) {
        final User user = userDao.findUser(username);
        if (user == null) {
            throw new RuntimeException(String.format("User %s not found", username));
        }
        return user;
    }


}
