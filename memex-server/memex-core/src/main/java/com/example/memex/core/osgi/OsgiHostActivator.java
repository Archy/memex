package com.example.memex.core.osgi;

import com.example.memex.api.osgi.MemexSystemBundleActivator;
import com.example.memex.api.osgi.PluginManager;
import com.example.memex.api.plugin.TitleFilter;
import com.example.memex.core.osgi.log.OsgiLoggerManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;


public class OsgiHostActivator implements MemexSystemBundleActivator {
    private static final Logger log = LogManager.getLogger(OsgiHostActivator.class);

    private static record OsgiServiceRef(Class<?> pluginPoint, Object service) {
    }

    private static final Class<?>[] PLUGIN_POINTS = new Class<?>[] {
            TitleFilter.class
    };

    private Bundle systemBundle;
    private BundleContext bundleContext;

    private final ServiceListener serviceListener;
    private final Map<ServiceReference<?>, OsgiServiceRef> serviceReference2service = new HashMap<>();
    private final OsgiLoggerManager osgiLoggerManager = new OsgiLoggerManager();
    private final PluginManager pluginManager;

    public OsgiHostActivator(PluginManager pluginManager) {
        this.pluginManager = pluginManager;
        serviceListener = event -> {
            log.debug("Service changed" + event);
            switch (event.getType()) {
                case ServiceEvent.REGISTERED -> {
                    log.debug("Registering service " + event);
                    registerService(event.getServiceReference());
                }
                case ServiceEvent.UNREGISTERING -> {
                    log.debug("Unregistering service " + event);
                    unregisterService(event.getServiceReference());
                }
                default -> {
                    final String msg = String.format("Service event %d is not supported", event.getType());
                    log.error(msg);
                    throw new RuntimeException(msg);
                }
            }
        };
    }

    @Override
    public void start(@NotNull final BundleContext context) {
        log.info("Starting host bundle");
        bundleContext = context;
        bundleContext.addServiceListener(serviceListener);
        osgiLoggerManager.start(bundleContext);
    }

    @Override
    public void stop(@NotNull final BundleContext context) {
        log.info("Stopping host bundle");
        osgiLoggerManager.stop();
        bundleContext.removeServiceListener(serviceListener);
    }

    @Override
    public void setSystemBundle(Bundle systemBundle) {
        this.systemBundle = systemBundle;
    }

    private void registerService(@NotNull final ServiceReference<?> serviceReference) {
        for (Class<?> pluginPoint: PLUGIN_POINTS) {
            if (serviceReference.isAssignableTo(systemBundle, pluginPoint.getName())) {
                log.info(String.format("Registering service %s as %s", serviceReference, pluginPoint.getName()));

                final Object service = bundleContext.getService(serviceReference);
                serviceReference2service.put(serviceReference, new OsgiServiceRef(pluginPoint, service));
                pluginManager.registerService(pluginPoint, service);
            }
        }
    }

    private void unregisterService(@NotNull final ServiceReference<?> serviceReference) {
        log.info(String.format("Registering service %s", serviceReference));
        final OsgiServiceRef serviceRef = checkNotNull(serviceReference2service.get(serviceReference),
                String.format("Service ref for %s is null", serviceReference));
        pluginManager.unregisterService(serviceRef.pluginPoint(), serviceRef.service());
        serviceReference2service.remove(serviceReference);
        bundleContext.ungetService(serviceReference);
    }
}
