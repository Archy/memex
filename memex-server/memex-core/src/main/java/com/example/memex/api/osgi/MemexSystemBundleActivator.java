package com.example.memex.api.osgi;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;

/**
 * MemeX OSGi system bundle activator
 * Handles plugins' services registration and OSGi logging config
 */
public interface MemexSystemBundleActivator extends BundleActivator {
    /**
     * set OSGi system bundle
     */
    void setSystemBundle(Bundle systemBundle);
}
