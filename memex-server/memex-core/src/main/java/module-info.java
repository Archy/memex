open module memex.core {
    exports com.example.memex.api.cluster;
    exports com.example.memex.api.locks;
    exports com.example.memex.api.meme;
    exports com.example.memex.api.meme.cache;
    exports com.example.memex.api.meme.processing;
    exports com.example.memex.api.osgi;
    exports com.example.memex.api.properties;
    exports com.example.memex.api.security;
    exports com.example.memex.api.storage;
    exports com.example.memex.api.transaction;
    exports com.example.memex.api.user;
    exports com.example.memex.api.vote;

    requires memex.api;
    requires memex.shared.api;

    //Spring
    requires spring.jms;

    //OSGi
    requires org.apache.felix.framework;
    requires org.osgi.service.log;

    //ActiveMQ
    requires activemq.client;
    requires jms;
    requires java.naming;

    // Hazelcast
    requires com.hazelcast.core;

    //Misc
    requires com.google.common;
    requires org.apache.logging.log4j;
    requires org.apache.commons.lang3;
    requires org.apache.httpcomponents.httpclient;
    requires org.jetbrains.annotations;
}