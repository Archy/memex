package com.example.memex.plugin.vulgarity.filter.impl;

import com.example.memex.api.plugin.TitleFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Hashtable;

/**
 * Naive vulgarity filter. It's not working correctly :)
 */
public class VulgarityFilterImpl implements BundleActivator, TitleFilter {
    private static final Logger log = LogManager.getLogger(VulgarityFilterImpl.class);

    private static final String[] VULGARITIES = new String[] {
            "ass", "fuck"
    };

    private ServiceRegistration<VulgarityFilterImpl> registration;

    @Override
    public void start(BundleContext bundleContext) {
        log.info("Vulgarity filer is starting");
        registration = bundleContext.registerService(
                VulgarityFilterImpl.class,
                new VulgarityFilterImpl(),
                new Hashtable<String, String>());
    }

    @Override
    public void stop(BundleContext bundleContext) {
        log.info("Vulgarity filer is stopping");
        registration.unregister();
    }

    @Override
    @NotNull
    public String filterTitle(@NotNull String title) {
        for (String vulgarity: VULGARITIES) {
            if (title.contains(vulgarity)) {
                log.debug(String.format("Filtering out %s from %s", vulgarity, title));
                title = title.replaceAll(vulgarity, "***");
            }
        }
        return title;
    }
}