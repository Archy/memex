package com.example.memex.plugin.title.filter.impl;

import com.example.memex.api.plugin.TitleFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Hashtable;

public class TitleFilterImpl implements BundleActivator, TitleFilter {
    private static final Logger log = LogManager.getLogger(TitleFilterImpl.class);

    private ServiceRegistration<TitleFilterImpl> registration;

    @Override
    public void start(BundleContext bundleContext) {
        log.info("Your mom tile filer is starting");
        registration = bundleContext.registerService(
                TitleFilterImpl.class,
                new TitleFilterImpl(),
                new Hashtable<String, String>());
    }

    @Override
    public void stop(BundleContext bundleContext) {
        log.info("Your mom tile filer is stopping");
        registration.unregister();
    }

    @Override
    @NotNull
    public String filterTitle(@NotNull String title) {
        log.debug("your mom");
        return title + " your mom";
    }
}
