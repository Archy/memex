import {castVote, loadVotes} from "/resources/js/vote-componenets.js";

function addEventListener(memeNode, memeId, selector, voteType) {
    const voteButton = memeNode.find(selector);
    voteButton.click(event => {
        event.preventDefault();
        castVote(memeId, memeNode, voteType);
    })
}

export default function initVoting() {
    console.log('dupa');

    const memes = $('.meme');
    for (let meme of memes) {
        const memeNode = $(meme);
        const memeId = memeNode.data('id');

        loadVotes(memeId, memeNode);
        addEventListener(memeNode, memeId, '.btn-success', 'upvote');
        addEventListener(memeNode, memeId, '.btn-danger', 'downvote');
    }
}