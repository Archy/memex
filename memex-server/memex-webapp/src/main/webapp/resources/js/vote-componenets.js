export function castVote(id, meme, type) {
    console.debug(`Casting vote type ${type} for ${id}`);

    const spinner = meme.find('.justify-content-end .fa-spinner');
    const buttons = meme.find('.justify-content-end .btn-group');
    spinner.show();
    buttons.hide();

    $.ajax({
            url: `/rest/api/vote/${id}?voteType=${type}`,
            method: 'POST'
        })
        .done(data => {
            spinner.hide();
            buttons.show();
            loadVotes(id, meme);
        })
        .fail(data => {
            console.error(data);
        });
}

export function loadVotes(id, meme) {
    console.debug('Loading vote for ', id);
    $.ajax(`/rest/api/vote/${id}`)
        .done(data=> {
            const spinner = meme.find('.lead > .fa-spinner');
            spinner.hide();

            const voteCount = data;
            const voteHolder =  meme.find('.lead > p');
            voteHolder.text(voteCount);
            voteHolder.show();
        })
        .fail(data => {
            console.error(data);
        })
}

