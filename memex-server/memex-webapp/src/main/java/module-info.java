module memex.webapp {
    requires memex.core;
    requires memex.persistence;
    requires memex.shared.api;
    requires memex.shared.core;

    //JavaEE
    requires javax.servlet.api;
    requires java.validation;

    //Spring
    requires spring.beans;
    requires spring.context;
    requires spring.core;
    requires spring.jms;

    requires spring.orm;
    requires spring.tx;

    requires spring.security.config;
    requires spring.security.core;
    requires spring.security.crypto;
    requires spring.security.web;

    requires spring.web;
    requires spring.webmvc;
    requires thymeleaf;
    requires thymeleaf.spring5;
    requires thymeleaf.extras.springsecurity5;

    //Persistence
    requires com.zaxxer.hikari;
    requires java.persistence;
    requires java.sql;

    //Jackson
    requires com.fasterxml.jackson.databind;

    //Misc
    requires com.google.common;
    requires org.apache.logging.log4j;
    requires org.apache.commons.lang3;
    requires org.jetbrains.annotations;
}