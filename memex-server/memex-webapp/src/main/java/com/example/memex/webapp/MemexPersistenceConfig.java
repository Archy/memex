package com.example.memex.webapp;

import com.example.memex.api.meme.MemeDao;
import com.example.memex.api.user.UserDao;
import com.example.memex.api.vote.VoteDao;
import com.example.memex.persistence.jpa.MemeJpaDao;
import com.example.memex.persistence.jpa.UserJpaDao;
import com.example.memex.persistence.jpa.VoteJpaDao;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.springframework.transaction.TransactionDefinition.ISOLATION_READ_COMMITTED;
import static org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRED;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:memex-persistence.properties")
public class MemexPersistenceConfig {
    private static final Logger log = LogManager.getLogger(MemexPersistenceConfig.class);

    @SuppressWarnings("ContextJavaBeanUnresolvedMethodsInspection") // close method is present in the HikariDataSource, but not in the DataSource interface
    @Bean(destroyMethod = "close")
    public DataSource dataSource(@Value("${memex.database.driver}") final String driver,
                          @Value("${memex.database.url}") final String url,
                          @Value("${memex.database.user}") final String user,
                          @Value("${memex.database.password}") final String password) {
        log.info("Initializing datasource");
        log.debug(String.format("Database driver: %s", driver));
        log.debug(String.format("Database url: %s", url));
        log.debug(String.format("Database user: %s", user));

        final HikariConfig config = new HikariConfig();
        config.setDriverClassName(driver);
        config.setJdbcUrl(url);
        config.setUsername(user);
        config.setPassword(password);
        return new HikariDataSource(config);
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    @Bean
    public Properties jpaProperties() throws IOException {
        final Properties properties = new Properties();
        final InputStream inputStream = getClass().getClassLoader().getResourceAsStream("hibernate.properties");
        if (inputStream == null) {
            throw new RuntimeException("hibernate.properties file not found");
        }
        properties.load(inputStream);

        return properties;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(final DataSource dataSource,
                                                                    final JpaVendorAdapter jpaVendorAdapter,
                                                                    final Properties jpaProperties) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        factoryBean.setPackagesToScan("com.example.memex.persistence.model",
                                      "com.example.memex.persistence.jpa.converter");
        factoryBean.setJpaProperties(jpaProperties);

        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    public TransactionTemplate transactionTemplate(PlatformTransactionManager platformTransactionManager) {
        final TransactionTemplate transactionTemplate = new TransactionTemplate(platformTransactionManager);
        transactionTemplate.setPropagationBehavior(PROPAGATION_REQUIRED);
        transactionTemplate.setIsolationLevel(ISOLATION_READ_COMMITTED);
        return transactionTemplate;
    }

    @Bean
    public MemeDao memeDao() {
        return new MemeJpaDao();
    }

    @Bean
    public UserDao userDao() {
        return new UserJpaDao();
    }

    @Bean
    public VoteDao voteDao() {
        return new VoteJpaDao();
    }
}
