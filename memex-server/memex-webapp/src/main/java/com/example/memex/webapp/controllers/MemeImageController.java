package com.example.memex.webapp.controllers;

import com.example.memex.api.meme.CachedMemeService;
import com.example.memex.api.meme.ImageManager;
import com.example.memex.api.meme.ImmutableMeme;
import com.example.memex.webapp.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;

@Controller
@RequestMapping("/image")
public class MemeImageController {

    private final ImageManager imageManager;
    private final CachedMemeService cachedMemeService;

    @Autowired
    public MemeImageController(final ImageManager imageManager, final CachedMemeService cachedMemeService) {
        this.imageManager = imageManager;
        this.cachedMemeService = cachedMemeService;
    }

    @GetMapping("/{filename:.+}")
    public @ResponseBody byte[] doGet(@PathVariable("filename") final String filename,
                                      @RequestParam(value = "token", required = false) String token) throws IOException {
        final ImmutableMeme meme = cachedMemeService.findByFilename(filename);
        ValidationUtils.validate(meme != null, "Meme not found");

        if (!meme.isVisible()) {
            ValidationUtils.validate(Objects.equals(meme.getAccessToken(), token), "Access token doesn't match");
        }

        final Optional<Path> file = imageManager.getMeme(filename);
        ValidationUtils.validate(file.isPresent(), "Image not found");

        return Files.readAllBytes(file.get());
    }
}
