package com.example.memex.webapp.controllers;

import com.example.memex.api.meme.CachedMemeService;
import com.example.memex.api.meme.ImmutableMeme;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class MemeXController {
    private static final Logger log = LogManager.getLogger(MemeXController.class);

    private final CachedMemeService cachedMemeService;

    @Autowired
    public MemeXController(final CachedMemeService cachedMemeService) {
        this.cachedMemeService = cachedMemeService;
    }

    @GetMapping
    public String get(final Model model) {
        final List<ImmutableMeme> memes = cachedMemeService.findAll().stream()
                .filter(ImmutableMeme::isVisible)
                .sorted((m1, m2) -> -Long.compare(m1.getId(), m2.getId()))
                .collect(Collectors.toList());
        model.addAttribute("memes", memes);
        return "index";
    }
}
