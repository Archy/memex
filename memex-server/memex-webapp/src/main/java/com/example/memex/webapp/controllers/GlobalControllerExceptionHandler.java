package com.example.memex.webapp.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
    private static final Logger log = LogManager.getLogger(GlobalControllerExceptionHandler.class);

    @ExceptionHandler(Throwable.class)
    public ModelAndView handleError(final HttpServletRequest req, final Exception ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);

        final ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
        mav.setViewName("fiveOhOh");
        return mav;
    }
}
