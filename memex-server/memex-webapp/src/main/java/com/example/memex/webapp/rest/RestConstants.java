package com.example.memex.webapp.rest;

public class RestConstants {
    public static final String REST = "rest";
    public static final String API = "api";
    public static final String REST_BASE_PATH = REST + "/" + API;

    public static final String VOTE = "vote";
    public static final String VOTE_TYPE = "voteType";
    public static final String MEME_ID = "memeId";
    public static final String HEALTH_CHECK = "healthCheck";
}
