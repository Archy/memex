package com.example.memex.webapp;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Enables Spring {@link org.springframework.web.filter.DelegatingFilterProxy}
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
