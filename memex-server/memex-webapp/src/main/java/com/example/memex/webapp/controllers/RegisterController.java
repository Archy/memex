package com.example.memex.webapp.controllers;

import com.example.memex.api.user.UserService;
import com.example.memex.webapp.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegisterController {
    private static final Logger log = LogManager.getLogger(RegisterController.class);

    private final UserService userService;

    @Autowired
    public RegisterController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String doGet(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping
    public String doPost(@Valid final User user, final BindingResult bindingResult, final Model model) {
        log.debug("Creating new user");
        if (userService.findUser(user.getUsername()) != null) {
            bindingResult.addError(new FieldError("user", "username", String.format("Username '%s' is already taken", user.getUsername())));
        }
        if (bindingResult.hasErrors()) {
            return "register";
        }

        userService.save(user.getUsername(), user.getPassword());

        return "redirect:/";
    }
}
