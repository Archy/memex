package com.example.memex.webapp.utils;

import com.example.memex.webapp.exceptions.ValidationException;

public class ValidationUtils {

    /**
     * Validates condition and throws {@link ValidationException} if condition is false
     *
     * @throws ValidationException if validation failed
     */
    public static void validate(final boolean condition, final String message) {
        if (!condition) {
            throw new ValidationException(message);
        }
    }

}
