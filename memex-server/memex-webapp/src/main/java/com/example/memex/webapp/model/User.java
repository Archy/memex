package com.example.memex.webapp.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {
    @NotNull
    @Size(min = 2, message = "Username is too short")
    private String username;
    @NotNull
    @Size(min = 3, message = "Password is too short")
    private String password;

    public User() {
    }

    public User(@NotNull @Size(min = 2) final String username, @NotNull @Size(min = 3) final String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
