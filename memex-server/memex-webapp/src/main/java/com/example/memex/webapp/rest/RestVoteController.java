package com.example.memex.webapp.rest;

import com.example.memex.api.vote.VoteService;
import com.example.memex.api.vote.VoteType;
import com.example.memex.webapp.utils.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(RestConstants.VOTE)
public class RestVoteController {
    private static final Logger log = LogManager.getLogger(RestVoteController.class);

    private final VoteService voteService;

    @Autowired
    public RestVoteController(final VoteService voteService) {
        this.voteService = voteService;
    }

    // curl -v -X GET -u admin:admin -H "Accept: application/json" http://localhost:8080/rest/api/vote
    @GetMapping("{" + RestConstants.MEME_ID + "}")
    public long getVotes(@PathVariable(RestConstants.MEME_ID) final long memeId) {
        return voteService.countVotes(memeId);
    }

    @PostMapping("{" + RestConstants.MEME_ID + "}")
    public void vote(@PathVariable(RestConstants.MEME_ID) final long memeId,
                     @RequestParam(RestConstants.VOTE_TYPE) final String voteTypeString,
                     final Principal principal) {
        final VoteType voteType = VoteType.fromString(voteTypeString);
        ValidationUtils.validate(voteType != null, String.format("Unrecognized vote type %s", voteTypeString));
        ValidationUtils.validate(principal != null, "You must be logged in to vote");

        log.debug(String.format("User %s votes %s on meme %d", principal.getName(), voteType, memeId));
        voteService.vote(memeId, principal.getName(), voteType);

    }

    // curl -X DELETE -u admin:admin http://localhost:8080/rest/api/vote/2
    @DeleteMapping("{" + RestConstants.MEME_ID + "}")
    public void deleteVotes(@PathVariable(RestConstants.MEME_ID) final long memeId,
                            final Principal principal) {
        log.debug(String.format("User %s removes his vote from %d", principal.getName(), memeId));
        voteService.removeVote(memeId, principal.getName());
    }
}
