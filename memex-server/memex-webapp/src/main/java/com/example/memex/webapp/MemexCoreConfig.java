package com.example.memex.webapp;

import com.example.memex.api.cluster.MemeXNode;
import com.example.memex.api.locks.ScopedExclusionService;
import com.example.memex.api.meme.*;
import com.example.memex.api.meme.cache.CacheInvalidator;
import com.example.memex.api.meme.processing.MemeProcessingService;
import com.example.memex.api.osgi.OsgiContainerManager;
import com.example.memex.api.osgi.PluginManager;
import com.example.memex.api.security.MemexPasswordEncoder;
import com.example.memex.api.storage.HomeDirectoryManager;
import com.example.memex.api.transaction.MemexTransactionManager;
import com.example.memex.api.user.UserDao;
import com.example.memex.api.user.UserService;
import com.example.memex.api.vote.VoteDao;
import com.example.memex.api.vote.VoteService;
import com.example.memex.core.cluster.MemeXHzNode;
import com.example.memex.core.locks.ClusterScopedExclusionService;
import com.example.memex.core.locks.LocalScopedExclusionService;
import com.example.memex.core.meme.ImageManagerImpl;
import com.example.memex.core.meme.MemeAccessServiceImpl;
import com.example.memex.core.meme.MemeProcessingListener;
import com.example.memex.core.meme.MemeServiceImpl;
import com.example.memex.core.meme.cache.CacheInvalidationListener;
import com.example.memex.core.meme.cache.CachedMemeServiceImpl;
import com.example.memex.core.meme.cache.JmsCacheInvalidator;
import com.example.memex.core.meme.cache.LocalCacheInvalidator;
import com.example.memex.core.meme.processing.AgentlessMemeProcessingService;
import com.example.memex.core.meme.processing.MemeProcessingServiceImpl;
import com.example.memex.core.osgi.OsgiContainerManagerImpl;
import com.example.memex.core.osgi.OsgiHostActivator;
import com.example.memex.core.osgi.PluginManagerImpl;
import com.example.memex.core.storage.HomeDirectoryManagerImpl;
import com.example.memex.core.user.UserServiceImpl;
import com.example.memex.core.vote.VoteServiceImpl;
import com.example.memex.shared.api.MemeSender;
import com.example.memex.shared.api.MemeXJMS;
import com.example.memex.shared.api.ProcessedMemeSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.function.Supplier;

@Configuration
public class MemexCoreConfig {

    @Configuration
    @PropertySource("classpath:memex.properties")
    static class Defaults {
    }

    // MemeX core

    @Bean
    public MemeAccessService memeAccessService(final MemeDao memeDao, final MemexTransactionManager memexTransactionManager) {
        return new MemeAccessServiceImpl(memeDao, memexTransactionManager);
    }

    @Bean
    public MemeService memeService(final CacheInvalidator cacheInvalidator,
                                   final ImageManager imageManager,
                                   final MemeAccessService memeAccessService,
                                   final MemeDao memeDao,
                                   final MemeProcessingService memeProcessingService,
                                   final MemexTransactionManager memexTransactionManager,
                                   final PluginManager pluginManager,
                                   final ScopedExclusionService<String> scopedExclusionService) {
        return new MemeServiceImpl(cacheInvalidator, imageManager, memeAccessService, memeDao, memeProcessingService, memexTransactionManager, pluginManager, scopedExclusionService);
    }

    @Bean
    public UserService userService(final PasswordEncoder passwordEncoder,
                                   final MemexTransactionManager memexTransactionManager,
                                   final UserDao userDao) {
        final MemexPasswordEncoder memexPasswordEncoder = new MemexPasswordEncoder() {
            private final PasswordEncoder delegate = passwordEncoder;

            @Override
            public String encodePassword(String password) {
                return delegate.encode(password);
            }
        };

        return new UserServiceImpl(memexPasswordEncoder, memexTransactionManager, userDao);
    }

    @Bean
    public VoteService voteService(final MemeDao memeDao,
                                   final UserDao userDao,
                                   final VoteDao voteDao,
                                   final MemexTransactionManager memexTransactionManager) {
        return new VoteServiceImpl(memeDao, userDao, voteDao, memexTransactionManager);
    }

    @Bean
    public ImageManager imageManager(final HomeDirectoryManager homeDirectoryManager) {
        return new ImageManagerImpl(homeDirectoryManager);
    }

    // MemeX cache

    @Bean
    public CachedMemeService cachedMemeService(final MemeAccessService memeAccessService) {
        return new CachedMemeServiceImpl(memeAccessService);
    }

    @Bean
    @Profile("!tests")
    public CacheInvalidator jmsCacheInvalidator(final CachedMemeService cachedMemeService,
                                                final JmsTemplate cacheInvalidationJmsTemplate,
                                                final MemeXJMS memeXJMS) {
        return new JmsCacheInvalidator(cachedMemeService, cacheInvalidationJmsTemplate, memeXJMS);
    }

    @Bean
    @Profile("!tests")
    public CacheInvalidationListener cacheInvalidationListener(final CachedMemeService cachedMemeService, final MemeXJMS memeXJMS) {
        return new CacheInvalidationListener(cachedMemeService, memeXJMS);
    }

    @Bean
    @Profile("tests")
    public CacheInvalidator localCacheInvalidator(final CachedMemeService cachedMemeService) {
        return new LocalCacheInvalidator(cachedMemeService);
    }

    // Locks:

    @Bean
    @Profile("!tests")
    public ScopedExclusionService<String> clusterScopedExclusionService(final MemeXNode memeXNode) {
        return new ClusterScopedExclusionService(memeXNode);
    }

    @Bean
    @Profile("tests")
    public ScopedExclusionService<String> localScopedExclusionService() {
        return new LocalScopedExclusionService<>();
    }

    // Cluster:

    @Bean
    @Profile("!tests")
    public MemeXNode memeXNode() {
        return new MemeXHzNode();
    }

    // Remote memes processing:

    @Bean("memeProcessingService")
    @Profile("tests")
    public MemeProcessingService agentlessMemeProcessingService(final CacheInvalidator cacheInvalidator,
                                                                final MemeDao memeDao,
                                                                final MemexTransactionManager memexTransactionManager,
                                                                @Value("${spring.profiles.active}") final String activeProfile) {
        return new AgentlessMemeProcessingService(cacheInvalidator, memeDao, memexTransactionManager, activeProfile);
    }

    @Bean("memeProcessingService")
    @Profile("!tests")
    public MemeProcessingService remoteMemeProcessingService(final MemeSender memeSender,
                                                             @Value("${memex.base.url}") final String memexUrl) {
        return new MemeProcessingServiceImpl(memeSender, memexUrl);
    }

    @Bean
    @Profile("!tests")
    public MemeProcessingListener memeProcessingListener(final MemeService memeService,
                                                         final MemeXJMS memeXJMS,
                                                         final ProcessedMemeSender processedMemeSender) {
        return new MemeProcessingListener(memeService, memeXJMS, processedMemeSender);
    }

    // Plugins:

    @Bean
    public PluginManager pluginManager() {
        return new PluginManagerImpl();
    }

    @Bean
    public OsgiContainerManager osgiContainerManager(final HomeDirectoryManager homeDirectoryManager,
                                                     final PluginManager pluginManager) {
        return new OsgiContainerManagerImpl(homeDirectoryManager, new OsgiHostActivator(pluginManager));
    }

    // Helper beans:

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public HomeDirectoryManager homeDirectoryManager(@Value("${memex.home}") final String homeDirectory,
                                                     @Value("${memex.local.home}") final String localHomeDirectory) {
        return new HomeDirectoryManagerImpl(homeDirectory, localHomeDirectory);
    }

    @Bean
    public MemexTransactionManager memexTransactionManager(final TransactionTemplate transactionTemplate) {
        return new MemexTransactionManager() {
            private final TransactionTemplate delegate = transactionTemplate;

            @Override
            public <T> T execute(Supplier<T> callback) {
                return delegate.execute(status -> callback.get());
            }

            @Override
            public void execute(Runnable callback) {
                delegate.execute(status -> {
                    callback.run();
                    return null;
                });
            }
        };
    }

}
