package com.example.memex.webapp.controllers;

import com.example.memex.api.meme.MemeService;
import com.example.memex.webapp.utils.ValidationUtils;
import com.google.common.net.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("/add")
public class MemeAddController {
    private static final Logger log = LogManager.getLogger(MemeAddController.class);

    private final MemeService memeService;

    @Autowired
    public MemeAddController(final MemeService memeService) {
        this.memeService = memeService;
    }

    @GetMapping
    public String doGet() {
        return "memeAdd";
    }

    @PostMapping
    public String doPost(@RequestParam final String title, @RequestParam final Part image) throws IOException {
        log.debug("Creating new user");

        ValidationUtils.validate(!StringUtils.isEmpty(title), "Title must not be empty");
        ValidationUtils.validate(image != null, "Image is missing");
        ValidationUtils.validate(image.getContentType().equals(MediaType.JPEG.toString()), "Image must be a jpg");

        try (final InputStream imageInputStream = image.getInputStream()) {
            memeService.save(title, imageInputStream);
        }

        return "redirect:/";
    }
}
