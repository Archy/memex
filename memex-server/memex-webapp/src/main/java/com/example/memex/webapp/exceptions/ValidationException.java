package com.example.memex.webapp.exceptions;

/**
 * Validation failed
 */
public class ValidationException extends RuntimeException {
    public ValidationException(final String message) {
        super(message);
    }
}
