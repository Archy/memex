package com.example.memex.webapp.spring;

import com.example.memex.webapp.rest.RestConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;

/**
 * Custom request mapping handler for easier rest controller configuration
 */
public class MemexRequestMappingHandlerMapping extends RequestMappingHandlerMapping {
    private static final Logger log = LogManager.getLogger(MemexRequestMappingHandlerMapping.class);

    @Override
    protected void registerHandlerMethod(@NotNull final Object handler,
                                         @NotNull final Method method,
                                         @NotNull RequestMappingInfo mapping) {
        final Class<?> beanType = method.getDeclaringClass();
        log.debug(String.format("method bean class is: %s", beanType));

        if (AnnotationUtils.findAnnotation(beanType, RestController.class) != null) {
            log.debug(String.format("Modifying mapping for %s as it's rest call", mapping));

            PatternsRequestCondition apiPattern = new PatternsRequestCondition(RestConstants.REST_BASE_PATH);
            final PatternsRequestCondition patternsRequestCondition = mapping.getPatternsCondition();
            if (patternsRequestCondition != null) {
                apiPattern = apiPattern.combine(patternsRequestCondition);
            }

            mapping = new RequestMappingInfo(mapping.getName(), apiPattern,
                                             mapping.getMethodsCondition(), mapping.getParamsCondition(),
                                             mapping.getHeadersCondition(), mapping.getConsumesCondition(),
                                             mapping.getProducesCondition(), mapping.getCustomCondition());
        }

        super.registerHandlerMethod(handler, method, mapping);
    }
}
