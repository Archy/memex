package com.example.memex.webapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record RestVote (
        @JsonProperty("meme") long memeId,
        @JsonProperty("vote") String voteType

) {
}
