package com.example.memex.webapp.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(RestConstants.HEALTH_CHECK)
public class HealthCheckController {
    @GetMapping
    public String checkServerStatus() {
        return "ok";
    }

}
