package com.example.memex.webapp.listeners;

import com.example.memex.api.meme.CachedMemeService;
import com.example.memex.api.meme.ImageManager;
import com.example.memex.api.meme.ImmutableMeme;
import com.example.memex.api.meme.MemeService;
import com.example.memex.api.osgi.OsgiContainerManager;
import com.example.memex.api.properties.MemeXProperties;
import com.example.memex.api.storage.HomeDirectoryManager;
import com.example.memex.api.transaction.MemexTransactionManager;
import com.example.memex.api.user.ImmutableUser;
import com.example.memex.api.user.UserService;
import com.example.memex.api.vote.VoteService;
import com.example.memex.api.vote.VoteType;
import com.example.memex.core.cluster.MemeXHzNode;
import com.example.memex.persistence.jpa.MemeJpaDao;
import com.example.memex.persistence.jpa.UserJpaDao;
import com.example.memex.persistence.jpa.VoteJpaDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.InputStream;
import java.util.function.Consumer;

import static org.springframework.web.context.WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE;

public class ContextInitListener implements ServletContextListener {
    private static final Logger log = LogManager.getLogger(ContextInitListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            log.info("Context initialized");
            final ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());

            if (MemeXProperties.isFreshInit()) {
                // Delete previous data
                final MemexTransactionManager transactionManager = applicationContext.getBean(MemexTransactionManager.class);
                transactionManager.execute(() -> {
                    applicationContext.getBean(VoteJpaDao.class).deleteAll();
                    applicationContext.getBean(MemeJpaDao.class).deleteAll();
                    applicationContext.getBean(UserJpaDao.class).deleteAll();
                });
            }

            // Initialize all services that need it
            accessBeanSafely(applicationContext, HomeDirectoryManager.class, HomeDirectoryManager::initialize);
            accessBeanSafely(applicationContext, ImageManager.class, ImageManager::initialize);
            accessBeanSafely(applicationContext, OsgiContainerManager.class, OsgiContainerManager::intializeOsgiContainer);
            accessBeanSafely(applicationContext, CachedMemeService.class, CachedMemeService::initialize);
            accessBeanSafely(applicationContext, MemeXHzNode.class, MemeXHzNode::initialize);

            if (MemeXProperties.isFreshInit()) {
                createInitialData(sce, applicationContext);
            }
        } catch (Exception e) {
            log.error("Error initializing MemeX", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Context destroyed");
        final AnnotationConfigWebApplicationContext applicationContext =
                (AnnotationConfigWebApplicationContext) sce.getServletContext().getAttribute(ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

        accessBeanSafely(applicationContext, OsgiContainerManager.class, OsgiContainerManager::stopOsgiContainer);
        accessBeanSafely(applicationContext, MemeXHzNode.class, MemeXHzNode::stop);
    }

    private <T> void accessBeanSafely(final ApplicationContext applicationContext,
                                      final Class<T> clazz,
                                      final Consumer<T> consumer) {
        try {
            final T bean = applicationContext.getBean(clazz);
            consumer.accept(bean);
        } catch (NoSuchBeanDefinitionException e) {
            log.trace("Bean " + clazz + " wasn't found", e);
        }
    }

    private void createInitialData(final ServletContextEvent sce,
                                   final ApplicationContext applicationContext) {
        final VoteService voteService = applicationContext.getBean(VoteService.class);
        final MemeService memeService = applicationContext.getBean(MemeService.class);
        final UserService userService = applicationContext.getBean(UserService.class);

        // Create users and memes
        final ImmutableUser admin = userService.save("admin", "admin");
        final ImmutableMeme adminMeme = createMeme(sce, memeService, "admin meme");

        final ImmutableUser joe = userService.save("joe", "joe");
        final ImmutableMeme joeMeme = createMeme(sce, memeService, "joe meme");

        final ImmutableUser moe = userService.save("moe", "moe");
        final ImmutableMeme moeMeme = createMeme(sce, memeService, "moe meme");

        // Create votes
        voteService.vote(adminMeme.getId(), admin.getUsername(), VoteType.UPVOTE);
        voteService.vote(joeMeme.getId(), admin.getUsername(), VoteType.DOWNVOTE);
        voteService.vote(moeMeme.getId(), admin.getUsername(), VoteType.UPVOTE);

        voteService.vote(adminMeme.getId(), joe.getUsername(), VoteType.UPVOTE);
        voteService.vote(joeMeme.getId(), joe.getUsername(), VoteType.DOWNVOTE);
        voteService.vote(moeMeme.getId(), joe.getUsername(), VoteType.UPVOTE);

        voteService.vote(adminMeme.getId(), moe.getUsername(), VoteType.UPVOTE);
        voteService.vote(joeMeme.getId(), moe.getUsername(), VoteType.DOWNVOTE);
        voteService.vote(moeMeme.getId(), moe.getUsername(), VoteType.DOWNVOTE);
    }

    private ImmutableMeme createMeme(final ServletContextEvent sce,
                                     final MemeService memeService,
                                     final String title) {
        try (final InputStream imageInputStream = sce.getServletContext().getResourceAsStream("resources/img/meme.jpg")) {
            return memeService.save(title, imageInputStream);
        } catch (Exception e) {
            log.error(e);
            throw new RuntimeException(e);
        }
    }
}
