package com.example.memex.webapp;

import com.example.memex.shared.core.MemexCoreJmsConfig;
import com.example.memex.shared.core.MemexSharedJmsConfig;
import com.example.memex.webapp.listeners.ContextInitListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class MemexWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    public void onStartup(@NotNull ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);
        // this is needed to ensure that org.springframework.web.context.ContextLoaderListener is executed before ContextInitListener
        servletContext.addListener(new ContextInitListener());
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
            MemeXWebAppConfig.class,
            MemeXSecurityConfig.class,
            MemexPersistenceConfig.class,
            MemexSharedJmsConfig.class,
            MemexCoreJmsConfig.class,
            MemexCoreConfig.class
        };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[0];
    }

    @Override
    @NotNull
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void customizeRegistration(@NotNull final ServletRegistration.Dynamic registration) {
        super.customizeRegistration(registration);
        registration.setMultipartConfig(new MultipartConfigElement(
                null,
                20971520,
                20971520,
                1048576));
    }
}
